"""Setup script
"""

from setuptools import setup

setup(
    name='auth_server',
    author='Sebastian Frelle Koch',
    author_email='sebastian.frelle@gmail.com',
    packages=[
        'auth_server',
        'auth_server.db',
        'auth_server.security',
        'auth_server.web',
    ],
    include_package_data=True,
    install_requires=[
        'flask>=0.12,<1',
        'mysqlclient>=1.3.10,<1.4',
        'bcrypt>=3.1.3',
        'pyjwt>=1.4.2',
	'gunicorn>=19.7.1',
    ],
)
