'''Config for security module of auth server'''

import os

SECURITY_CONFIG = {
    'secret': os.getenv('SECRET_KEY', 'mastermindisawesome'),
    'algorithm': 'HS256',
}