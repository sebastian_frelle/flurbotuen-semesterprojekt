""" Used for generating and verifying tokens
"""

import datetime

import jwt

from .config import SECURITY_CONFIG as conf


class TokenError(Exception):
    """Defines some arbitrary error with the token
    """
    pass


def decode_token(token):
    """Verify a token
    """

    try:
        return jwt.decode(
            jwt=token,
            key=conf['secret'],
            algorithm=conf['algorithm'],
        )
    except jwt.ExpiredSignatureError:
        raise TokenError('Signature expired. Log in again.')
    except jwt.InvalidTokenError:
        raise TokenError('Invalid token. Log in again.')


def generate_token(user):
    """Get a JWT that holds the user info
    """

    payload = {
        'sub': user.id,
        'username': user.username,
        'admin': user.admin,
        'exp': datetime.datetime.utcnow() + datetime.timedelta(hours=2),
        'iat': datetime.datetime.utcnow()
    }

    return jwt.encode(
        payload=payload,
        key=conf['secret'],
        algorithm=conf['algorithm']
    )
