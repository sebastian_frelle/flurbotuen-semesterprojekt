"""Module used by table models for inserting new records and fetching old ones.
"""

import MySQLdb

from . import config

class AuthDatabase:
    """Used for connection and cursor to the database.
    """
    def __init__(self):
        self.db_creds = config.MYSQLDB_CREDENTIALS
        self.conn = None
        self.cur = None

    def connection(self):
        """Initialize connection if non-existent, then return
        """
        if self.conn is None or self.conn.closed:
            self.conn = self._init_db(self.db_creds)

        return self.conn

    def cursor(self):
        """Initialize and return cursor
        """
        return self.connection().cursor()

    def _init_db(self, credentials):
        return MySQLdb.connect(**credentials)
