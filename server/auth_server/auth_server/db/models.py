"""Database table models
"""

import bcrypt
from MySQLdb import Error, Warning

from .authdb import AuthDatabase

db = AuthDatabase()


class MultipleMatchesException(Exception):
    pass


class NoMatchException(Exception):
    pass


class SQLException(Exception):
    pass


class User:
    @staticmethod
    def checkpw(pw, user):
        return bcrypt.checkpw(
            pw.encode('utf-8'),
            user.passwordsha256.encode('utf-8'),
        )

    @classmethod
    def find_by_id(cls, uid):
        query = "SELECT * FROM User WHERE id = %s"

        with db.cursor() as cur:
            try:
                cur.execute(query, [uid])
                result = cur.fetchone()
            except Error as ex:
                print(ex)
                raise SQLException('Query error')
            finally:
                db.connection().close()

        if result is None:
            raise NoMatchException(f"No user found for user id {uid}")

        user = cls(**result)
        return user

    @classmethod
    def find_by_username(cls, username):
        query = "SELECT * FROM User WHERE username = %s"

        with db.cursor() as cur:
            try:
                cur.execute(query, [username])
                result = cur.fetchone()
            except Error as ex:
                print(ex)
                raise SQLException('Query error')
            finally:
                db.connection().close()

        if result is None:
            raise NoMatchException(f"No user found for username {username}")

        user = cls(**result)
        return user

    @classmethod
    def find(cls):
        query = "SELECT * FROM User"

        with db.cursor() as cur:
            try:
                cur.execute(query)
                results = cur.fetchall()
            except (Error, Warning):
                raise SQLException('Query error')
            finally:
                db.connection().close()

        if not len(results):
            raise NoMatchException(f'No users were found.')

        users_mapped = [cls(**r) for r in results]
        return users_mapped

    @classmethod
    def delete(cls, id):
        query = f"DELETE FROM User WHERE id = {id}"

        with db.cursor() as cur:
            try:
                result = cur.execute(query)
                db.connection().commit()
            except (Error, Warning) as ex:
                print(ex)
                db.connection().rollback()
                raise SQLException()
            finally:
                db.connection().close()

        if result == 0:
            raise NoMatchException("No user was found with id {id}.")

    @classmethod
    def register(cls, username, password, admin=False):
        salt = bcrypt.gensalt(12, prefix=b'2a')
        passwordsha256 = bcrypt.hashpw(
            password.encode('utf-8'),
            salt,
        )

        query = f"""INSERT INTO User(username, passwordsha256, salt, admin)
                    VALUES (%s, %s, %s, %s)"""

        params = (username, passwordsha256, salt, int(admin))

        with db.cursor() as cur:
            try:
                cur.execute(query, params)
                db.connection().commit()
                player_id = cur.lastrowid
            except (Error, Warning) as ex:
                print(ex)
                db.connection().rollback()
                raise SQLException()
            finally:
                db.connection().close()

        return player_id

    def __init__(self, id=None, username=None,
                 password=None, salt=None, passwordsha256=None, admin=False):
        self.id = id
        self.username = username
        self.password = password
        self.salt = salt
        self.passwordsha256 = passwordsha256
        self.admin = admin

    def set_password(self, new_password):
        if not isinstance(new_password, str):
            raise Exception('New password must be a string')

        self.password = new_password

        try:
            self.salt = bcrypt.gensalt(12, prefix=b'2a')  # generate new salt for security purposes
            self.passwordsha256 = bcrypt.hashpw(
                self.password.encode('utf-8'),
                self.salt,
            )
        except Exception as ex:
            print(ex)

    def hashpw(self):
        return bcrypt.hashpw(
            self.password.encode('utf-8'),
            self.salt
        )

    def save(self):
        query = """
                INSERT INTO 
                    User(username, passwordsha256, salt, admin)
                    VALUES(%s, %s, %s, %s)
                    ON DUPLICATE KEY UPDATE 
                        username=%s,
                        passwordsha256=%s,
                        salt=%s,
                        admin=%s
                """

        params = 2 * (
            self.username,
            self.passwordsha256,
            self.salt,
            int(self.admin),  # False=>0, True=>1
        )

        with db.cursor() as cur:
            try:
                cur.execute(query, params)
                db.connection().commit()
                player_id = cur.lastrowid
            except (Error, Warning) as ex:
                print(ex)
                db.connection().rollback()
                raise SQLException('db error')
            finally:
                db.connection().close()

        return player_id

    def desc(self):
        return {
            'id': self.id,
            'username': self.username,
            'admin': self.admin,
        }
