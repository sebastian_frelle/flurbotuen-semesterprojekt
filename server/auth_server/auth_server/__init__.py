"""Defines the API

This module serves as the definition of the admin REST API component of the
Flurbotuen Mastermind Multiplayer App. The API controls auth as well as CRUD
operations for admin users.

Routes are defined as follows:
    * Non-secure routes-- open to anyone.
    * Secure, non-admin routes-- open to authorized users without admin 
    privileges.
    * Secure, admin routes-- open to authorized users with admin
    privileges.

"""

from flask import (
    Flask,
    request,
    jsonify,
    make_response,
    g,
)

from functools import wraps

from .db.models import (
    User,
    MultipleMatchesException,
    NoMatchException,
    SQLException,
)

from .security import security as sec

app = Flask(__name__)


@app.route('/')
def greet():
    res = {
        'status': 'success',
        'msg': 'Welcome to the Flurbomind auth server!'
    }
    return jsonify(res)


@app.route('/login', methods=['POST'])
def login():
    request_data = request.get_json()

    try:
        user = User.find_by_username(request_data['username'])
        password = request_data['password']
    except (MultipleMatchesException, NoMatchException):
        res = {
            'status': 'fail',
            'msg': 'User not found.'
        }
        return make_response(jsonify(res), 404)
    except KeyError:
        res = {
            'status': 'fail',
            'msg': 'Needs username and password.'
        }
        return make_response(jsonify(res), 400)

    if not User.checkpw(password, user):
        res = {
            'status': 'fail',
            'msg': "Password did not match."
        }
        return make_response(jsonify(res), 400)

    res = {
        'status': 'success',
        'jwt': sec.generate_token(user).decode('utf-8')
    }
    return jsonify(res)


@app.route('/register', methods=['POST'])
def register():
    req_data = request.get_json()
    try:
        created_id = User.register(
            username=req_data['username'],
            password=req_data['password'],
        )
    except KeyError:
        res = {
            'status': 'fail',
            'msg': 'Needs username and password.'
        }
        return make_response(jsonify(res), 400)
    except SQLException:
        res = {
            'status': 'fail',
            'msg': 'Error on user creation.',
        }
        return make_response(jsonify(res), 500)
    
    res = {
        'status': 'success',
        'msg': f"Created user with username {req_data['username']} and id {created_id}.",
    }
    return jsonify(res)


#
# SECURED ROUTES
#

# verification decorator
def verify_jwt(func):
    @wraps(func)
    def wrapped_f(*args, **kwargs):
        try:
            token = request.headers['x-access-token']
            auth_payload = sec.decode_token(token)
        except KeyError:
            res = {
                'status': 'fail',
                'msg': 'Header x-access-token required.'
            }
            return make_response(jsonify(res), 400)
        except sec.TokenError as ex:
            res = {
                'status': 'fail',
                'msg': str(ex)
            }
            return make_response(jsonify(res), 403)

        g.sub = auth_payload['sub']
        g.username = auth_payload['username']
        g.admin = auth_payload['admin']

        return func(*args, **kwargs)

    return wrapped_f


# verify user is admin
def admin(f):
    @wraps(f)
    def wrapped_f(*args, **kwargs):
        if getattr(g, 'admin', False):
            return f(*args, **kwargs)
        else:
            res = {
                'status': 'fail',
                'msg': 'Needs admin privileges.'
            }
            return make_response(jsonify(res), 403)
    
    return wrapped_f


@app.route('/change_password', methods=['POST'])
@verify_jwt
def change_password():
    req_data = request.get_json()

    try:
        user = User.find_by_id(g.sub)
    except (MultipleMatchesException, NoMatchException) as ex:
        res = {
            'status': 'fail',
            'msg': 'User not found.'
        }
        return make_response(jsonify(res), 404)

    try:
        old_pw = req_data['old_password']
        new_pw = req_data['new_password']
        new_pw_repeat = req_data['new_password_repeat']
    except KeyError:
        res = {
            'status': 'fail',
            'msg': 'Needs old_password, new_password, new_password_repeat.'
        }
        return make_response(jsonify(res), 400)
    
    # Check old password
    if not User.checkpw(old_pw, user):
        res = {
            'status': 'fail',
            'msg': 'Password incorrect.',
        }
        return make_response(jsonify(res), 400)
    
    # Match new password entries
    if not new_pw == new_pw_repeat:
        res = {
            'status': 'fail',
            'msg': "New passwords don't match."
        }
        return make_response(jsonify(res), 400)

    # Set new password and save
    user.set_password(new_pw)
    try:
        user.save()
    except SQLException:
        res = {
            'status': 'fail',
            'msg': "Error on saving user."
        }
        return make_response(jsonify(res), 500)

    res = {
        'status': 'success',
        'msg': "User password changed."
    }
    return jsonify(res)


@app.route('/user')
@verify_jwt
def user():
    print('/user called')

    username = request.args.get('username')
    uid = request.args.get('id')

    if not username and not uid:
        res = {
            'status': 'fail',
            'msg': 'Needs username or id as a query parameter.'
        }
        return make_response(jsonify(res), 400)
    
    try:
        if not uid:
            user = User.find_by_username(username)
        else:
            user = User.find_by_id(uid)
    except NoMatchException:
        res = {
            'status': 'fail',
            'msg': 'User not found.'
        }
        return make_response(jsonify(res), 404)
    
    res = {
        'status': 'success',
        'user': user.desc()
    }
    return jsonify(res)


@app.route('/users')
@verify_jwt
def users():
    print("/users called")
    
    try:
        users = User.find()
    except NoMatchException:
        res = {
            'status': 'fail',
            'msg': 'No users found.'
        }
        return make_response(jsonify(res), 404)

    res = {
        'status': 'success',
        'users': [u.desc() for u in users]
    }
    return jsonify(res)


@app.route('/admin/user/<int:uid>', methods=['DELETE'])
@verify_jwt
@admin
def delete_user(uid):
    print(f'deleting user with id {uid}')
    try:
        User.delete(uid)
    except NoMatchException as ex:
        res = {
            'status': 'fail',
            'msg': f'User with id {uid} not found'
        }
        return make_response(jsonify(res), 404)
    except SQLException:
        res = {
            'status': 'fail',
            'msg': f'failed to delete user with id {uid}.'
        }
        return make_response(jsonify(res), 400)
    
    res = {
        'status': 'success',
        'msg': f'Deleted user with id {uid}.'
    }
    return jsonify(res)


@app.route('/admin/create_user', methods=['POST'])
@verify_jwt
@admin
def create_user():
    print('/admin/create_user called')
    req_data = request.get_json()

    try:
        created_id = User.register(
            username=req_data['username'],
            password=req_data['password'],
            admin=req_data['admin']
        )
    except KeyError:
        res = {
            'status': 'fail',
            'msg': 'Needs username, password, and admin true/false.',
        }
        return make_response(jsonify(res), 400)
    except SQLException:
        res = {
            'status': 'fail',
            'msg': 'Error on user creation.',
        }
        return make_response(jsonify(res), 500)
    
    res = {
        'status': 'success',
        'msg': f'User registered with id {created_id}.'
    }
    return jsonify(res)


@app.route('/admin/change_password/<int:uid>', methods=['POST'])
@verify_jwt
@admin
def admin_change_password(uid):
    req_data = request.get_json()

    try:
        user = User.find_by_id(uid)
    except (MultipleMatchesException, NoMatchException):
        res = {
            'status': 'fail',
            'msg': f'User with id {uid} not found.'
        }
        return make_response(jsonify(res), 404)
    
    try:
        old_pw = req_data['old_password']
        new_pw = req_data['new_password']
        new_pw_repeat = req_data['new_password_repeat']
    except KeyError:
        res = {
            'status': 'fail',
            'msg': 'Needs old_password, new_password, new_password_repeat.'
        }
        return make_response(jsonify(res), 400)

    # Check old password
    if not User.checkpw(old_pw, user):
        res = {
            'status': 'fail',
            'msg': 'Password incorrect.'
        }
        return make_response(jsonify(res), 400)
    
        # Match new password entries
    if not new_pw == new_pw_repeat:
        res = {
            'status': 'fail',
            'msg': "New passwords don't match."
        }
        return make_response(jsonify(res), 400)

    # Set new password and save
    user.set_password(new_pw)
    try:
        user.save()
    except SQLException:
        res = {
            'status': 'fail',
            'msg': "Error on saving user."
        }
        return make_response(jsonify(res), 500)

    res = {
        'status': 'success',
        'msg': "User password changed."
    }
    return jsonify(res)


@app.route('/admin/set_admin/<int:uid>', methods=['POST'])
@verify_jwt
@admin
def set_admin(uid):
    print('/admin/set_admin called')
    req_data = request.get_json()

    try:
        user = User.find_by_id(uid)
    except NoMatchException:
        res = {
            'status': 'fail',
            'msg': f'User with id {uid} not found.'
        }
        return make_response(jsonify(res), 404)
    
    try:
        user.admin = req_data['admin']        
    except KeyError:
        res = {
            'status': 'fail',
            'msg': 'Request needs admin true/false.'
        }
        return make_response(jsonify(res), 400)
    
    try:
        user.save()
    except SQLException:
        res = {
            'status': 'fail',
            'msg': 'Failed while saving user.'
        }
        return make_response(jsonify(res), 500)

    res = {
        'status': 'success',
        'msg': f'User with id {uid} modified and saved.'
    }
    return jsonify(res)

