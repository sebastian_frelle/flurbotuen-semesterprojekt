# Notes for routing, security, models, etc.

## Routing

List of routes/functionality to be included in the RESTful service.

### Todo

* non-protected routes
    * ~~greet~~
    * ~~login~~
    * ~~register~~
* protected – non-admin
    * ~~change_password~~
    * ~~user~~
    * ~~users~~
* protected – admin
    * ~~create user~~
    * delete user
    * update user (should really only be allowed to update password)

 
### Non-protected routes

* `greet`
* `login`
    * A user with admin privileges can provide their username and password in exchange for a token which grants them access to the secured routes.
* `/register`, `POST`
    * register a new, non-admin user

### Protected routes – non-admin

#### `/change_password`, `POST`

User provides a token and data to the format of

```json
{
    "oldpw": "...",
    "newpw": "...",
    "newpw2": "..."
}
```

Then, the server checks whether the token is valid and whether the new passwords match. If everything checks out, the new password replaces the old one in the database, and the user is logged out.

#### `/user`, `POST`

Allows the client to supply any information on the user and have the server sift through the database for a matching user, essentially allowing the client to do a selection query on all data on all users in the database. This might not be a problem/bad design, but it sure feels like it.

**Possible fixes:** Introduce an endpoint for GETting a user by id and another for GETting a user by username.

#### `/users`, `GET`

Return a list of all users in JSON format. Each entry should be of the format

```javascript
{
    "id": /* uid */,
    "username": /* username */,
    "admin": /* true/false */
}
```

Any other data to provide? Not really. The only other data which could be provided would be the hashed password and the salt, and providing those together especially would be a huge liability.

### Protected routes – admin only

#### `/user/id`, `DELETE`

Delete user with id `id`.

#### `/admin/register`, `POST`

Register a user with or without admin privileges-- see `/register`. User with admin privileges provides token and data on the form

```javascript
{
    "username": /* username of new user */,
    "password": /* password of new user */,
    "admin": /* true/false */
}
```

The client receives status along with the id of the newly created user.

## Auth

### Token-based authentication

The service implements token-based authentication with users logging in being provided with a `jwt` with payload of the form

```javascript
{
    "sub": /* username of user */,
    "userid": /* id of user */,
    "admin": /* true/false */,
    "iat": /* time of issue */,
    "exp": /* time until token expires */
}
```

This token is HS256-hashed with the key that's provided in the app configuration and returned to the user.

### Ideas for improvement

* Keep a token blacklist and add the token of any user that changes his/her password, has their admin rights revoked or whatever else might be up that would require them to log back into the system.