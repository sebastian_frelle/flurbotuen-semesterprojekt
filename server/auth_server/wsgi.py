"""WSGI entry point
"""

from auth_server import app

if __name__ == "__main__":
    app.run()
