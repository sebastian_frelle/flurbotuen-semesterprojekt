# Auth server for Mastermind

This flask-based server provides the Mastermind backend with authentication for the users attempting login that are *not* registered with the `Brugerautorisation` service.

This is a prototype. Currently, this app hasn't been built for distribution, which means it is only available in development mode. Later on, deployment to a gUnicorn server-- using nginx as a reverse proxy if deemed necessary-- will be added.

## Installation

This app requires Python 3. The following assumes that Python 3 is run using `python3` and that its pip installation is run using `pip3`. Additionally, this app requires a running MySQL server on `localhost` and admin privileges. A script is provided for creating the template database for the server to use. The database connection configuration can be found in the `authdb` directory.

It is recommended to use a virtual environment while working on this app.

### Development

1. Go to the root folder of this app
2. Create a virtual environment. This app requires python3.6 (I think), so use

```bash
$ python3 -m venv .env/
```

We're using `.env/`, since that is the name registered in `.gitignore`.

3. Source the virtual environment with

```bash
$ source .env/bin/activate
```

4. Install the app and its dependencies with

```bash
$ pip3 install --editable .
```

5. (Optional) Install production dependencies using the `requirements.txt` file.

```bash
$ pip3 install -r requirements.txt
```

6. Set the Flask environment variables and run the application.

```bash
$ export FLASK_APP=auth_server
$ export FLASK_DEBUG=True
$ flask run
```

Now, you should be able to access the server on `http://127.0.0.1:5000/`. When you're done working on the application, deactivate the virtual environment with 

```bash
$ deactivate
``` 

or by deleting the `.env/` directory.

## Authors

* Sebastian Frelle Koch <[sebastian.frelle@gmail.com](mailto:sebastian.frelle@gmail.com)\>