-- Assumes current user is root and that a
-- database named flurbotuen-mastermind exists.

-- BEWARE: executing this script wipes 
-- flurbotuen_mastermind db of all existing data

-- author: sebastian.frelle@gmail.com

USE flurbotuen_mastermind;
DROP TABLE IF EXISTS User;

CREATE TABLE User (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    username VARCHAR(255) NOT NULL UNIQUE,
    passwordsha256 VARCHAR(255) NOT NULL,
    salt VARCHAR(255) NOT NULL,
    admin TINYINT NOT NULL DEFAULT 0,
    CONSTRAINT PK_PlayerID PRIMARY KEY (id)
);

-- server application user
DROP USER IF EXISTS 'mastermindadmin'@'localhost'; -- assumes MySQL version >= 5.7.8

CREATE USER 'mastermindadmin'@'localhost'
    IDENTIFIED BY 'glutenfluen';

GRANT ALL ON flurbotuen_mastermind.* TO 'mastermindadmin'@'localhost';