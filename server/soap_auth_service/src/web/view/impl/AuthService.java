package web.view.impl;


import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import model.dao.IUserDao;
import model.dao.impl.UserDao;
import model.dao.models.User;
import service.SecurityService;
import web.dto.components.Status;
import web.dto.request.*;
import web.dto.response.*;
import web.view.IAuthService;

import javax.annotation.Resource;
import javax.jws.WebService;
import javax.xml.ws.WebServiceContext;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

@WebService(endpointInterface = "web.view.IAuthService")
public class AuthService implements IAuthService {
    private static IUserDao userDao = new UserDao();

    @Resource
    private WebServiceContext ctx;

    @Override
    public BasicStatusDto greet(GreetDto greetDto) {
        System.out.println("greet called");

        String msg = greetDto.getMsg();

        if (msg != null) {
            return new BasicStatusDto(Status.SUCCESS, "Here is your message: " + msg);
        }

        return new BasicStatusDto(Status.SUCCESS,
                "Welcome to the Flurbomind SOAP-based user admin service!");
    }

    @Override
    public LoginStatusDto login(LoginDto loginDto) {
        System.out.println("login called");

        System.out.println(userDao);
        User user;
        try {
            user = userDao.findByUsername(loginDto.getUsername());
        } catch (SQLException e) {
            e.printStackTrace();
            return new LoginStatusDto(
                    Status.FAIL,
                    "Could not find user"
            );
        }

        if (!SecurityService.checkPassword(loginDto.getPassword(), user.getPasswordSha256())) {
            return new LoginStatusDto(
                    Status.FAIL,
                    "Passwords don't match"
            );
        }

        String token = "";
        try {
            token = user.getJwt();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            System.exit(1);
        } catch (JWTCreationException e) {
            e.printStackTrace();
            return new LoginStatusDto(
                    Status.FAIL,
                    "Token creation error: " + e.getMessage()
            );
        }

        return new LoginStatusDto(
                Status.SUCCESS,
                "Successfully logged in",
                token
        );
    }

    @Override
    public BasicStatusDto register(RegisterDto registerDto) {
        System.out.println("register called");

        long createdId;
        try {
            createdId = userDao.createUser(registerDto.getUsername(), registerDto.getPassword(), false);
        } catch (SQLException e) {
            e.printStackTrace();
            return new BasicStatusDto(
                    Status.FAIL,
                    "Creating user failed"
            );
        }

        return new RegisterStatusDto(
                Status.SUCCESS,
                "Creating user with id " + createdId + " succeeded",
                createdId
        );
    }

    @Override
    public BasicStatusDto changePassword(String token, ChangePasswordDto changePasswordDto) {
        System.out.println("changePassword called");
        DecodedJWT decodedToken;
        try {
            decodedToken = SecurityService.verifyJwt(token);
        } catch (JWTVerificationException e) {
            return new BasicStatusDto(
                    Status.FAIL,
                    "Could not verify token. Log in again."
            );
        }

        User user;
        Claim sub = decodedToken.getClaim("sub");
        Long id = Long.valueOf(sub.asString());

        String oldPassword = changePasswordDto.getOldPassword();
        String newPassword = changePasswordDto.getNewPassword();
        String newPasswordRetype = changePasswordDto.getNewPasswordRetype();
        try {
            user = userDao.findById(id);
        } catch (SQLException e) {
            e.printStackTrace();
            return new BasicStatusDto(
                    Status.FAIL,
                    "Could not find user with id " + id
            );
        }

        if (!SecurityService.checkPassword(oldPassword, user.getPasswordSha256())) {
            return new BasicStatusDto(
                    Status.FAIL,
                    "Old password doesn't match."
            );
        }

        if (!newPassword.equals(newPasswordRetype)) {
            return new BasicStatusDto(
                    Status.FAIL,
                    "New passwords don't match."
            );
        }

        String newPasswordHashed = SecurityService.hashPassword(newPassword, user.getSalt());
        user.setPasswordSha256(newPasswordHashed);

        try {
            userDao.saveUser(user);
        } catch (SQLException e) {
            e.printStackTrace();
            return new BasicStatusDto(
                    Status.FAIL,
                    "Failed to save user with error msg: " + e.getMessage()
            );
        }

        return new BasicStatusDto(
                Status.SUCCESS,
                "User password updated"
        );
    }

    @Override
    public GetUserResponseDto getUser(String token, GetUserDto getUserDto) {
        System.out.println("getUser called");

        try {
            SecurityService.verifyJwt(token);
        } catch (JWTVerificationException e) {
            return new GetUserResponseDto(
                    Status.FAIL,
                    "Could not verify token. Log in again."
            );
        }

        Long id = getUserDto.getId();
        String username = getUserDto.getUsername();

        if (id == null && username == null) {
            return new GetUserResponseDto(
                    Status.FAIL,
                    "Needs either id or username."
            );
        }

        User user;
        try {
            user = (id != null) ? userDao.findById(id) : userDao.findByUsername(username);
        } catch (SQLException e) {
            return new GetUserResponseDto(
                    Status.FAIL,
                    "User not found"
            );
        }

        UserResponseDto userResponseDto = new UserResponseDto();
        userResponseDto.setId(user.getId());
        userResponseDto.setUsername(user.getUsername());
        userResponseDto.setAdmin(user.getAdmin());

        return new GetUserResponseDto(
                Status.SUCCESS,
                "User found for parameters",
                userResponseDto
        );
    }

    @Override
    public GetUsersResponseDto getUsers(String token) {
        System.out.println("getUsers called");

        try {
            SecurityService.verifyJwt(token);
        } catch (JWTVerificationException e) {
            return new GetUsersResponseDto(
                    Status.FAIL,
                    "Could not verify token. Log in again."
            );
        }

        List<User> users;
        try {
            users = userDao.find();
        } catch (SQLException e) {
            e.printStackTrace();
            return new GetUsersResponseDto(
                    Status.FAIL,
                    "Internal error. Finding users failed."
            );
        }

        List<UserResponseDto> userResponseDtos = users.stream()
                .map(u -> new UserResponseDto(u.getId(), u.getUsername(), u.getAdmin()))
                .collect(Collectors.toList());

        return new GetUsersResponseDto(
                Status.SUCCESS,
                "Users found: " + userResponseDtos.size(),
                userResponseDtos
        );
    }

    @Override
    public DeleteUserStatusDto adminDeleteUser(String token, Long id) {
        System.out.println("adminDeleteUser called");

        DecodedJWT decodedJWT;
        try {
            decodedJWT = SecurityService.verifyJwt(token);
        } catch (JWTVerificationException e) {
            return new DeleteUserStatusDto(
                    Status.FAIL,
                    "Could not verify token. Log in again."
            );
        }

        Boolean isAdmin = decodedJWT.getClaim("admin").asBoolean();
        if (!isAdmin) {
            return new DeleteUserStatusDto(
                    Status.FAIL,
                    "User does not have admin rights"
            );
        }

        try {
            userDao.deleteUserById(id);
        } catch (SQLException e) {
            e.printStackTrace();
            return new DeleteUserStatusDto(
                    Status.FAIL,
                    "Could not delete user. err: " + e.getMessage()
            );
        }

        return new DeleteUserStatusDto(
                Status.SUCCESS,
                "Deleted user with id " + id
        );
    }

    @Override
    public BasicStatusDto adminCreateUser(String token, AdminCreateUserDto adminCreateUserDto) {
        System.out.println("adminCreateUser called");

        DecodedJWT decodedJWT;
        try {
            decodedJWT = SecurityService.verifyJwt(token);
        } catch (JWTVerificationException e) {
            return new BasicStatusDto(
                    Status.FAIL,
                    "Could not verify token. Log in again."
            );
        }

        Boolean isAdmin = decodedJWT.getClaim("admin").asBoolean();
        if (!isAdmin) {
            return new BasicStatusDto(
                    Status.FAIL,
                    "User does not have admin rights"
            );
        }

        long createdId;
        try {
            createdId = userDao.createUser(adminCreateUserDto.getUsername(),
                    adminCreateUserDto.getPassword(),
                    adminCreateUserDto.getAdmin());
        } catch (SQLException e) {
            e.printStackTrace();
            return new BasicStatusDto(
                    Status.FAIL,
                    "Creating user failed"
            );
        }

        return new RegisterStatusDto(
                Status.SUCCESS,
                "Creating user with id " + createdId + " succeeded",
                createdId
        );
    }

    @Override
    public BasicStatusDto adminChangePassword(String token, AdminChangePasswordDto adminChangePasswordDto) {
        DecodedJWT decodedJWT;
        try {
            decodedJWT = SecurityService.verifyJwt(token);
        } catch (JWTVerificationException e) {
            return new BasicStatusDto(
                    Status.FAIL,
                    "Could not verify token. Log in again."
            );
        }

        Boolean isAdmin = decodedJWT.getClaim("admin").asBoolean();
        if (!isAdmin) {
            return new BasicStatusDto(
                    Status.FAIL,
                    "User does not have admin rights."
            );
        }

        User user;
        Long id = adminChangePasswordDto.getId();
        String oldPassword = adminChangePasswordDto.getOldPassword();
        String newPassword = adminChangePasswordDto.getNewPassword();
        String newPasswordRetype = adminChangePasswordDto.getNewPasswordRetype();
        try {
            user = userDao.findById(id);
        } catch (SQLException e) {
            e.printStackTrace();
            return new BasicStatusDto(
                    Status.FAIL,
                    "Could not find user with id " + id
            );
        }

        if (!SecurityService.checkPassword(oldPassword, user.getPasswordSha256())) {
            return new BasicStatusDto(
                    Status.FAIL,
                    "Old password doesn't match."
            );
        }

        if (!newPassword.equals(newPasswordRetype)) {
            return new BasicStatusDto(
                    Status.FAIL,
                    "New passwords don't match."
            );
        }

        String newPasswordHashed = SecurityService.hashPassword(newPassword, user.getSalt());
        user.setPasswordSha256(newPasswordHashed);

        try {
            userDao.saveUser(user);
        } catch (SQLException e) {
            e.printStackTrace();
            return new BasicStatusDto(
                    Status.FAIL,
                    "Failed to save user with error msg: " + e.getMessage()
            );
        }

        return new BasicStatusDto(
                Status.SUCCESS,
                "User password updated"
        );
    }

    @Override
    public BasicStatusDto adminSetAdmin(String token, AdminSetAdminDto adminSetAdminDto) {
        DecodedJWT decodedJWT;
        try {
            decodedJWT = SecurityService.verifyJwt(token);
        } catch (JWTVerificationException e) {
            return new BasicStatusDto(
                    Status.FAIL,
                    "Could not verify token. Log in again."
            );
        }

        Boolean isAdmin = decodedJWT.getClaim("admin").asBoolean();
        if (!isAdmin) {
            return new BasicStatusDto(
                    Status.FAIL,
                    "User does not have admin rights."
            );
        }

        User user;
        Long id = adminSetAdminDto.getId();
        Boolean admin = adminSetAdminDto.getAdmin();
        try {
            user = userDao.findById(id);
        } catch (SQLException e) {
            return new BasicStatusDto(
                    Status.FAIL,
                    "Could not find user with id " + id
            );
        }

        user.setAdmin(admin);

        try {
            userDao.saveUser(user);
        } catch (SQLException e) {
            return new BasicStatusDto(
                    Status.FAIL,
                    "Failed to save user with id " + id
            );
        }

        return new BasicStatusDto(
                Status.SUCCESS,
                "Set admin rights to " + admin + " on user with id " + id
        );
    }
}