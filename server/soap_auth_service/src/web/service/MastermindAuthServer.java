package web.service;

import web.view.IAuthService;
import web.view.impl.AuthService;

import javax.xml.ws.Endpoint;

public class MastermindAuthServer {
    public static void main(String[] args) {
        System.out.print("Publishing auth service... ");
        IAuthService authService = new AuthService();

        Endpoint.publish("http://[::]:9071/auth", authService);
        System.out.println("Auth service successfully published");
    }
}
