package web.dto.response;

import web.dto.components.Status;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "status")
public class BasicStatusDto {
    private Status status;
    private String msg;

    public BasicStatusDto() {}

    public BasicStatusDto(Status status, String msg) {
        this.status = status;
        this.msg = msg;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
