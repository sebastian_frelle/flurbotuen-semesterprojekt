package web.dto.response;

import web.dto.components.Status;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "loginstatus")
public class LoginStatusDto extends BasicStatusDto {
    private String jwt;

    public LoginStatusDto() {}

    public LoginStatusDto(Status status, String msg) {
        super(status, msg);
    }

    public LoginStatusDto(Status status, String msg, String jwt) {
        super(status, msg);
        this.jwt = jwt;
    }

    public String getJwt() {
        return jwt;
    }

    public void setJwt(String jwt) {
        this.jwt = jwt;
    }
}
