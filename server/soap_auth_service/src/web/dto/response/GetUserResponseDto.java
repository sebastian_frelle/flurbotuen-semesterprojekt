package web.dto.response;

import web.dto.components.Status;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "userData")
public class GetUserResponseDto extends BasicStatusDto {
    private UserResponseDto user;

    public GetUserResponseDto() {}

    public GetUserResponseDto(Status status, String msg) {
        super(status, msg);
    }

    public GetUserResponseDto(UserResponseDto user) {
        this.user = user;
    }

    public GetUserResponseDto(Status status, String msg, UserResponseDto user) {
        super(status, msg);
        this.user = user;
    }

    public UserResponseDto getUser() {
        return user;
    }

    public void setUser(UserResponseDto user) {
        this.user = user;
    }
}
