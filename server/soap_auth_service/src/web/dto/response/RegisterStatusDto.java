package web.dto.response;

import web.dto.components.Status;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "registerstatus")
public class RegisterStatusDto extends BasicStatusDto {
    private Long id;

    public RegisterStatusDto(Long id) {
        this.id = id;
    }

    public RegisterStatusDto(Status status, String msg, Long id) {
        super(status, msg);
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
