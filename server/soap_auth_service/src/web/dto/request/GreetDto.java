package web.dto.request;

import javax.xml.bind.annotation.*;

@XmlRootElement
@XmlType
@XmlAccessorType(XmlAccessType.FIELD)
public class GreetDto {
    @XmlElement(name = "msg")
    private String msg;

    public GreetDto() {}

    public GreetDto(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
