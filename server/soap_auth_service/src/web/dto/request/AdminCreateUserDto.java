package web.dto.request;

import javax.xml.bind.annotation.*;

@XmlRootElement
@XmlType
@XmlAccessorType(XmlAccessType.FIELD)
public class AdminCreateUserDto {
    @XmlElement(required = true)
    private String username;

    @XmlElement(required = true)
    private String password;

    @XmlElement(required = true)
    private Boolean admin;

    public AdminCreateUserDto() {}

    public AdminCreateUserDto(String username, String password, Boolean admin) {
        this.username = username;
        this.password = password;
        this.admin = admin;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getAdmin() {
        return admin;
    }

    public void setAdmin(Boolean admin) {
        this.admin = admin;
    }
}
