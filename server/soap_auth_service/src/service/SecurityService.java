package service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.mindrot.jbcrypt.BCrypt;

import java.io.UnsupportedEncodingException;

public class SecurityService {
    public static String generateSalt() {
        return BCrypt.gensalt(12);
    }

    public static boolean checkPassword(String password, String hashedPassword) {
        return BCrypt.checkpw(password, hashedPassword);
    }

    public static String hashPassword(String password, String salt) {
        return BCrypt.hashpw(password, salt);
    }

    public static String getSecret() {
        String secret = System.getenv("SECRET_KEY");
        if (secret == null) {
            System.err.println("SECRET_KEY environment variable not set; using default.");
            secret = "mastermindisawesome";
        }

        return secret;
    }

    public static DecodedJWT verifyJwt(String token) throws JWTVerificationException {
        String secret = getSecret();
        Algorithm algorithm = null;
        try {
            algorithm = Algorithm.HMAC256(secret);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            System.exit(1);
        }

        JWTVerifier jwtVerifier = JWT.require(algorithm).build();
        return jwtVerifier.verify(token);
    }
}
