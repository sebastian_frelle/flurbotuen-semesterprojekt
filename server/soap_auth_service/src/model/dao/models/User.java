package model.dao.models;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import service.SecurityService;

import java.io.UnsupportedEncodingException;
import java.util.Date;

public class User {
    private Long id;
    private String username,
            password,
            passwordSha256,
            salt;
    private Boolean admin;

    public User() {}

    public User(Long id, String username, String password, String passwordSha256, String salt, Boolean admin) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.passwordSha256 = passwordSha256;
        this.salt = salt;
        this.admin = admin;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordSha256() {
        return passwordSha256;
    }

    public void setPasswordSha256(String passwordSha256) {
        this.passwordSha256 = passwordSha256;
    }

    public Boolean getAdmin() {
        return admin;
    }

    public void setAdmin(Boolean admin) {
        this.admin = admin;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getJwt() throws UnsupportedEncodingException {
        String secret = SecurityService.getSecret();
        Algorithm algorithm = Algorithm.HMAC256(secret);
        Date issueTime = new Date();
        Date expiresTime = new Date(issueTime.getTime() + 720000); // 2 hours expiration time

        return JWT.create()
                .withIssuedAt(issueTime)
                .withExpiresAt(expiresTime)
                .withSubject("" + this.getId())
                .withClaim("username", this.getUsername())
                .withClaim("admin", this.getAdmin())
                .sign(algorithm);
    }
}
