package model.dao;

import model.dao.models.User;

import java.sql.SQLException;
import java.util.List;

public interface IUserDao {
    List<User> find() throws SQLException;

    User findById(Long id) throws SQLException;

    User findByUsername(String username) throws SQLException;

    Long createUser(String username, String password, Boolean admin) throws SQLException;

    Long saveUser(User user) throws SQLException;

    void deleteUserById(Long id) throws SQLException;
}
