package model.dao;

import model.models.Credentials;

import java.sql.SQLException;

public interface ICredentialsDao {
    Long createCredentials(Credentials credentials) throws SQLException;
    Credentials getOneByUsername(String username) throws SQLException;
}
