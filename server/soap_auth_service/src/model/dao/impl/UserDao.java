package model.dao.impl;

import model.config.Connector;
import model.dao.IUserDao;
import model.dao.models.User;
import service.SecurityService;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class UserDao implements IUserDao {
    @Override
    public List<User> find() throws SQLException {
        PreparedStatement stmt;
        String query = "SELECT * FROM User";
        stmt = Connector.getConnection().prepareStatement(query);
        ResultSet rs = stmt.executeQuery();
        List<User> users = new ArrayList<>();

        while (rs.next()) {
            users.add(mapResultToUser(rs));
        }

        return users;
    }

    @Override
    public User findByUsername(String username) throws SQLException {
        PreparedStatement stmt;
        String query = "SELECT * FROM User WHERE username = ?";

        stmt = Connector.getConnection().prepareStatement(query);
        stmt.setString(1, username);

        ResultSet rs = stmt.executeQuery();
        rs.next();
        User user = mapResultToUser(rs);

        if (rs.next()) { // check if more than one row was found
            throw new SQLException("More than one result found for username " + username);
        }

        return user;
    }

    @Override
    public User findById(Long id) throws SQLException {
        PreparedStatement stmt;
        String query = "SELECT * FROM User WHERE id = ?";

        stmt = Connector.getConnection().prepareStatement(query);
        stmt.setLong(1, id);

        ResultSet rs = stmt.executeQuery();
        rs.next();
        User user = mapResultToUser(rs);

        if (rs.next()) { // check if more than one row was found
            throw new SQLException("More than one result found for user id " + id);
        }

        return user;
    }

    private User mapResultToUser(ResultSet rs) throws SQLException {
        User user = new User();
        user.setId(rs.getLong("id"));
        user.setUsername(rs.getString("username"));
        user.setPasswordSha256(rs.getString("passwordsha256"));
        user.setSalt(rs.getString("salt"));
        user.setAdmin(rs.getBoolean("admin"));

        return user;
    }

    @Override
    public Long createUser(String username, String password, Boolean admin) throws SQLException {
        User user = new User();
        user.setUsername(username);
        user.setPassword(password);
        user.setSalt(SecurityService.generateSalt());
        user.setPasswordSha256(SecurityService.hashPassword(password, user.getSalt()));
        user.setAdmin(admin);

        String query = "INSERT INTO User(username, passwordsha256, salt, admin) VALUES(?, ?, ?, ?)";
        PreparedStatement stmt = Connector.getConnection().prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
        stmt.setString(1, user.getUsername());
        stmt.setString(2, user.getPasswordSha256());
        stmt.setString(3, user.getSalt());
        stmt.setLong(4, (user.getAdmin()) ? 1 : 0);

        stmt.executeUpdate();

        ResultSet generatedKeys = stmt.getGeneratedKeys();
        generatedKeys.next();
        return generatedKeys.getLong(1);
    }

    @Override
    public Long saveUser(User user) throws SQLException {
        String query = "INSERT INTO User(username, passwordsha256, salt, admin) VALUES(?, ?, ?, ?) " +
                "ON DUPLICATE KEY UPDATE " +
                "username = VALUES(username), " +
                "passwordsha256 = VALUES(passwordsha256), " +
                "salt = VALUES(salt), " +
                "admin = VALUES(admin)";

        PreparedStatement stmt = Connector.getConnection().prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
        stmt.setString(1, user.getUsername());
        stmt.setString(2, user.getPasswordSha256());
        stmt.setString(3, user.getSalt());
        stmt.setLong(4, (user.getAdmin()) ? 1 : 0);

        stmt.executeUpdate();

        ResultSet generatedKeys = stmt.getGeneratedKeys();
        if (generatedKeys.next()) {
            return generatedKeys.getLong(1);
        }

        return user.getId();
    }

    @Override
    public void deleteUserById(Long id) throws SQLException {
        String query = "DELETE FROM User WHERE id = ?";
        PreparedStatement stmt = Connector.getConnection().prepareStatement(query);
        stmt.setLong(1, id);

        stmt.executeUpdate();
    }
}
