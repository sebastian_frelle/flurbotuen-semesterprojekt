package model.dao.impl;

import model.config.Connector;
import model.dao.ICredentialsDao;
import model.models.Credentials;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class CredentialsDao implements ICredentialsDao {
    @Override
    public Long createCredentials(Credentials credentials) throws SQLException {
        PreparedStatement stmt;
        String query = "INSERT INTO " +
                "Credentials(Username, PasswordSHA256, Salt) VALUES(?, ?, ?)";

        stmt = Connector.getConnection().prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
        stmt.setString(1, credentials.getUsername());
        stmt.setString(2, credentials.getHashedPassword());
        stmt.setString(3, credentials.getSalt());
        int affectedRows = stmt.executeUpdate();

        System.out.println("Credentials updated, " + affectedRows + " rows affected.");

        try (ResultSet generatedKeys = stmt.getGeneratedKeys()) {
            if (generatedKeys.next()) {
                return generatedKeys.getLong(1);
            } else {
                throw new SQLException("Creating credentials failed");
            }
        }
    }

    @Override
    public Credentials getOneByUsername(String username) throws SQLException {
        PreparedStatement stmt;
        String query = "SELECT * FROM Credentials WHERE Username = ?";
        stmt = Connector.getConnection().prepareStatement(query);
        stmt.setString(1, username);

        ResultSet rs = stmt.executeQuery();
        rs.next(); // position cursor on first row

        Credentials credentials = new Credentials();
        credentials.setId(rs.getLong("id"));
        credentials.setUsername(rs.getString("Username"));
        credentials.setHashedPassword(rs.getString("PasswordSHA256"));
        credentials.setSalt(rs.getString("Salt"));

        if (rs.next()) { // check if more than one row was found
            throw new SQLException("More than one result found for username " + username);
        }

        return credentials;
    }
}
