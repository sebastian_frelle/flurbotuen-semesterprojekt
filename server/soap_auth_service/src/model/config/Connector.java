package model.config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Connector {
    private static final String HOST = "127.0.0.1";
    private static final int PORT = 3306;
    private static final String DATABASE = "flurbotuen_mastermind";
    private static final String USERNAME = "mastermindadmin";
    private static final String PASSWORD = "glutenfluen";
    private static Connection connection = null;

    private static String dbUrl() {
        return String.format(
                "jdbc:mysql://%s:%d/%s?user=%s&password=%s&autoReconnect=true",
                HOST, PORT, DATABASE, USERNAME, PASSWORD
        );
    }

    private static void connect() {
        try {
            connection = DriverManager.getConnection(dbUrl());
        } catch (SQLException e) {
            System.out.println("SQLException: " + e.getMessage());
            System.out.println("SQL State: " + e.getSQLState());
            System.out.println("VendorError: " + e.getErrorCode());
            System.exit(1);
        }
    }

    public static Connection getConnection() {
        if (connection == null) {
            connect();
        }

        return connection;
    }
}
