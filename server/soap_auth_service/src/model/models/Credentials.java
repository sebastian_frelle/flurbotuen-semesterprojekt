package model.models;

public class Credentials {
    private Long id;
    private String username,
            password,
            hashedPassword,
            salt;

    public Credentials(String username, String password, String hashedPassword, String salt) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.hashedPassword = hashedPassword;
        this.salt = salt;
    }

    public Credentials() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getHashedPassword() {
        return hashedPassword;
    }

    public void setHashedPassword(String hashedPassword) {
        this.hashedPassword = hashedPassword;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }
}
