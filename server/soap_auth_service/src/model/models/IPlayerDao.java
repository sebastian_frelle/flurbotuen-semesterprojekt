package model.models;

import java.sql.SQLException;

public interface IPlayerDao {
    boolean createPlayer(Player player) throws SQLException;
}
