package model.models.impl;

import model.config.Connector;
import model.models.IPlayerDao;
import model.models.Player;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class PlayerDao implements IPlayerDao {

    @Override
    public boolean createPlayer(Player player) throws SQLException {
        PreparedStatement stmt = null;
        String query = "INSERT INTO Player(LastName, FirstName, EmailAddress, Credentials) VALUES(?, ?, ?, ?)";

        stmt = Connector.getConnection().prepareStatement(query);
        stmt.setString(1, player.getLastName());
        stmt.setString(2, player.getFirstName());
        stmt.setString(3, player.getEmailAddress());
        stmt.setLong(4, player.getCredentialsId());

        int affectedRows = stmt.executeUpdate();

        if (affectedRows > 1) {
            throw new SQLException("Something went wrong, " + affectedRows + " rows updated.");
        }

        System.out.println("Player table updated, " + affectedRows + " rows affected.");
        return true;
    }
}
