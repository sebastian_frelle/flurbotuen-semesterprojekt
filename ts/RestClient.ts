/// <reference path="HTTP.ts" />

namespace RestClient {
    const url: string = location.protocol + '//' + location.hostname + (location.port ? ':' + location.port : '') + '/api';
    
    export function register(param: {username: string, password: string}, onDone: (res: HTTP.Response) => void): void {
        HTTP.post(url + '/register', param, [], onDone);
    }

    export function login(param: {username: string, password: string}, onDone: (res: HTTP.Response) => void): void {
        HTTP.post(url + '/login', param, [], onDone);
    }

    export function getUser(token: string, onDone: (res: HTTP.Response) => void): void {
        HTTP.get(url + '/getUser', [{name: 'x-access-token', value: token}], onDone);
    }

    export function hostGame(token: string, onDone: (res: HTTP.Response) => void): void {
        HTTP.get(url + '/host', [{name: 'x-access-token', value: token}], onDone);
    }

    export function joinGame(token: string, gameid: number | string, onDone: (res: HTTP.Response) => void): void {
        HTTP.post(url + '/join', {gameid: gameid}, [{name: 'x-access-token', value: token}], onDone);
    }

    export function start(token: string, gameid: number | string, onDone: (res: HTTP.Response) => void): void {
        HTTP.post(url + '/start', {gameid: gameid}, [{name: 'x-access-token', value: token}], onDone);
    }

    export function wait(token: string, gameid: number | string, onDone: (res: HTTP.Response) => void): void {
        HTTP.post(url + '/wait', {gameid: gameid}, [{name: 'x-access-token', value: token}], onDone);
    }

    export function submit(token: string, gameid: number | string, guess: number[], onDone: (res: HTTP.Response) => void): void {
        HTTP.post(url + '/submit', {gameid: gameid, guess: guess}, [{name: 'x-access-token', value: token}], onDone);
    }

    export function update(token: string, gameid: number | string, onDone: (res: HTTP.Response) => void) {
        HTTP.post(url + '/update', {gameid: gameid}, [{name: 'x-access-token', value: token}], onDone);
    }

    export function leave(token: string, gameid: number | string, onDone: (res: HTTP.Response) => void): void {
        HTTP.post(url + '/leave', {gameid: gameid}, [{name: 'x-access-token', value: token}], onDone);
    }

    export function getHostedGames(token: string, onDone: (res: HTTP.Response) => void): void {
        HTTP.get(url + '/hostlist', [{name: 'x-access-token', value: token}], onDone);
    }

    export function getHostedGame(token: string, gameid: number | string, onDone: (res: HTTP.Response) => void): void {
        HTTP.get(url + '/getHostedGame/' + gameid, [{name: 'x-access-token', value: token}], onDone);
    }

    export function getUserName(token: string, uid: string, onDone: (res: HTTP.Response) => void): void {
        HTTP.get(url + '/getUserName/' + uid, [{name: 'x-access-token', value: token}], onDone);
    }

    export function ping(token: string, onDone: (res: HTTP.Response) => void): void {
        HTTP.get(url + '/ping', [{name: 'x-access-token', value: token}], onDone);
    }
}