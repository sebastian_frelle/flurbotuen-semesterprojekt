namespace HTTP {
    export interface Response {
        code: number,
        text: string
    }

    export interface Header {
        name: string,
        value: string
    }
    
    export function get(url: string, headers: Header[], callback: (res: Response) => void): void {
        let req: XMLHttpRequest = new XMLHttpRequest();

        req.open('get', url, true);
        addHeaders(req, headers);
        req.send();

        req.onreadystatechange = function() {
            if(req.readyState === XMLHttpRequest.DONE) {
                let res: Response = {
                    code: req.status,
                    text: req.responseText
                };

                callback(res);
            }
        };
    }

    export function post(url: string, param: {}, headers: Header[], callback: (res: Response) => void): void {
        let req: XMLHttpRequest = new XMLHttpRequest();

        req.open('post', url, true);
        req.setRequestHeader('Content-Type', 'application/json');

        addHeaders(req, headers);

        if(param) {
            req.send(JSON.stringify(param));
        } else {
            req.send();
        }

        req.onreadystatechange = function() {
            if(req.readyState === XMLHttpRequest.DONE) {
                let res: Response = {
                    code: req.status,
                    text: req.responseText
                };

                callback(res);
            }
        };
    }

    function addHeaders(req: XMLHttpRequest, headers: Header[]): void {
        for(let header of headers) {
            req.setRequestHeader(header.name, header.value);
        }
    }
}