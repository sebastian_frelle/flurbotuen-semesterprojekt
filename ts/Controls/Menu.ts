/// <reference path="Control.ts" />
/// <reference path="Link.ts" />

class Menu extends Control {
    private element: HTMLUListElement;
    private menuItems: Link[];

    constructor(element: HTMLUListElement) {
        super();

        this.element = element;
        this.menuItems = [];
    }

    public getElement(): HTMLElement {
        return this.element;
    }

    public addMenuItem(item: Link): void {
        this.menuItems.push(item);
        let li = document.createElement('li');
        li.appendChild(item.getElement());
        this.element.appendChild(li);
    }

    public removeMenuItem(item: Link): void {
        for(let i = 0; i < this.menuItems.length; i++) {
            if(this.menuItems[i] === item) {
                this.element.removeChild(item.getElement().parentElement);
                this.menuItems.splice(i, 1)[0].dispose();
                break;
            }
        }
    }

    public dispose(): void {
        for(let item of this.menuItems) {
            item.dispose();
        }

        this.element = null;
    }
}