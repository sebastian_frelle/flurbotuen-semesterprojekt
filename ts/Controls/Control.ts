abstract class Control {
        constructor() {}

    public abstract getElement(): HTMLElement;

    public show(): void {
        this.getElement().classList.remove('hidden');
    }

    public hide(): void {
        this.getElement().classList.add('hidden');
    }

    public abstract dispose(): void;
}