/// <reference path="Control.ts" />

class Table extends Control {
    private element: HTMLTableElement;
    private addedRows: HTMLElement[];

    constructor(element: HTMLTableElement) {
        super();
        this.element = element;
        this.addedRows = [];
    }

    public getElement(): HTMLElement {
        return this.element;
    }

    public addRow(cols: (string | HTMLElement)[]): void {
        if(cols.length === 0) {
            return;
        }

        let tr = this.element.insertRow();

        for(let col of cols) {
            let td = tr.insertCell();

            if(col instanceof HTMLElement) {
                td.appendChild(col);
            } else {
                td.textContent = col;
            }
        }

        this.addedRows.push(tr);
    }

    public getRowElement(index: number): HTMLElement {
        if(index >= this.addedRows.length || index < 0) {
            throw new Error('index out of bounds');
        }
        
        return this.addedRows[index];
    }

    public clearAddedRows(): void {
        while(this.addedRows.length > 0) {
            this.element.deleteRow(this.element.rows.length - 1);
            this.addedRows.pop();
        }
    }

    public dispose(): void {
        this.element = null;
    }
}