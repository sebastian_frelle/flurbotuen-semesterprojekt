/// <reference path="Control.ts" />

class Link extends Control {
    private element: HTMLAnchorElement;
    public onClick: (e: Event) => void;

    constructor(element: HTMLAnchorElement) {
        super();

        this.element = element;
        this.element.href="javascript:;";
        this.element.addEventListener('click', this.clicked);
    }

    public getElement(): HTMLElement {
        return this.element;
    }

    private clicked = (e: Event): void => {
        this.onClick(e);
    };

    public dispose(): void {
        this.element.removeEventListener('click', this.clicked);
        this.element = null;
    }
}