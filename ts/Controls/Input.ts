/// <reference path="Control.ts" />

class Input extends Control {
    private element: HTMLInputElement;
    public onEnter: (e: KeyboardEvent) => void;

    constructor(element: HTMLInputElement) {
        super();

        this.element = element;

        this.element.addEventListener('keyup', this.onKeyUp);
    }

    private onKeyUp = (e: KeyboardEvent): void => {
        if(e.which === 13 && this.onEnter) {
            this.onEnter(e);
        }
    };

    public getElement(): HTMLElement {
        return this.element;
    }

    public getValue(): any {
        return this.element.value;
    }

    public dispose(): void {
        this.element.removeEventListener('click', this.onKeyUp);
        this.element = null;
    }
}