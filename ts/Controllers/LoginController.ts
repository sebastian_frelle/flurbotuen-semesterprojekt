/// <reference path="Controller.ts" />
/// <reference path="../Controls/Input.ts" />
/// <reference path="../Controls/Link.ts" />
/// <reference path="../RestClient.ts" />
/// <reference path="../SPA.ts" />

class LoginController extends Controller {
    private email: Input;
    private password: Input;
    private login: Link;
    
    constructor(container: HTMLElement) {
        super(container);

        let submit = (e: Event) => {
            RestClient.login({
                username: this.email.getValue(), 
                password: this.password.getValue()
            }, function(res: HTTP.Response) {
                if(res.code === 200) {
                    Session.setToken(res.text);
                    SPA.popToHome();
                } else {
                    alert('Code: ' + res.code + '\n' + res.text);
                }
            });
        };

        this.email = new Input(<HTMLInputElement>this.getElementByControlId('email'));
        this.password = new Input(<HTMLInputElement>this.getElementByControlId('password'));
        this.login = new Link(<HTMLAnchorElement>this.getElementByControlId('login'));

        this.email.onEnter = submit;
        this.password.onEnter = submit;
        this.login.onClick = submit;
    }

    public onActivated(): void {
        console.log('LoginController onActivated()');
    }

    public clean(): void  {
        this.login.dispose();
        this.email.dispose();
        this.password.dispose();
    }
}