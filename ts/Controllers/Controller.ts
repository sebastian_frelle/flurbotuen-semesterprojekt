class Controller {
    private container: HTMLElement;
    private controlElements: {[key: string]: HTMLElement};

    constructor(container: HTMLElement) {
        if(this.constructor === Controller) {
            throw new Error('Controller cannot be directly instanciated.');
        }

        this.container = container;
        this.controlElements = {};

        this.parseContainer(<HTMLElement>this.container);
    }

    private parseContainer(element: HTMLElement): void {
        if(element.classList.contains('control')) {
            let cid = element.getAttribute('data-cid');

            if(!cid) {
                throw new Error(element.outerHTML + "\nElement has no data-cid attribute.");
            }

            this.controlElements[cid] = element;
        }

        for(let i = 0; i < element.children.length; i++) {
            let child = element.children[i];
            this.parseContainer(<HTMLElement>child);
        }
    }

    public getContainer(): HTMLElement {
        return this.container;
    }

    public getElementByControlId(controlId: string): HTMLElement {
        return this.controlElements[controlId];
    }

    public onActivated(): void {}

    public onDestroyed(): void {}

    public clean(): void {}
}