/// <reference path="Controller.ts" />
/// <reference path="../Controls/Table.ts" />
/// <reference path="../RestClient.ts" />

class UserInfoController extends Controller {
    private userTable: Table;
    
    constructor(container: HTMLElement) {
        super(container);

        this.userTable = new Table(<HTMLTableElement>this.getElementByControlId('user-table'));
    }

    public onActivated(): void {
        console.log('UserInfoController onActivated');

        RestClient.getUser(Session.getToken(), (res: HTTP.Response) => {
            if(res.code === 200) {
                let data = JSON.parse(res.text);
                this.userTable.addRow([data.wins, data.losses]);
            } else {
                alert('Code: ' + res.code + '\n' + res.text);
            }
        });
    }

    public clear(): void {
        this.userTable.dispose();
    }
}