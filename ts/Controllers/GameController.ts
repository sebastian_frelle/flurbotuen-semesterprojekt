/// <reference path="Controller.ts" />
/// <reference path="../Controls/Table.ts" />
/// <reference path="../Controls/Link.ts" />
/// <reference path="../Session.ts" />
/// <reference path="../RestClient.ts" />

class GameController extends Controller {
    private static readonly CSS_COLORS = [
        'blue',
        'green',
        'lightblue',
        'yellow',
        'red',
        'purple',
        'grey',
        'black',
    ];

    private static readonly CSS_UNSET_COLOR = 'lightgrey';
    private static readonly CSS_CURRENT_TURN = 'current-turn';
    private static readonly CSS_CORRECT_COLOR = 'yellow';
    private static readonly CSS_CORRECT_PLACE = 'red';
    private static readonly NUM_TURNS = 10;

    private paletteTable: Table;
    private paletteButtons: Link[];
    private currentColor: number;

    private opponentPins: Table;
    
    private boardTable: Table;

    private boardButtons: Link[][];
    private boardPins: HTMLSpanElement[][];

    private submitButton: Link;

    private currentTurn: number;
    private currentGuess: number[];
    private updating: boolean;

    constructor(container: HTMLElement) {
        super(container);

        this.paletteTable = new Table(<HTMLTableElement>this.getElementByControlId('palette'));
        this.paletteButtons = [];
        this.currentColor = -1;

        this.opponentPins = new Table(<HTMLTableElement>this.getElementByControlId('opponent'));

        this.boardTable = new Table(<HTMLTableElement>this.getElementByControlId('board'));
        this.boardButtons = [];
        this.boardPins = [];

        this.submitButton = new Link(<HTMLAnchorElement>this.getElementByControlId('submit'));
        this.submitButton.onClick = this.submit;

        this.currentTurn = 0;
        this.currentGuess = [-1, -1, -1, -1];
        this.updating = false;

        this.createOpponentPins();
        this.createPalette();
        this.createBoard();
    }
    
    private submit = (e: Event): void => {
        if(this.updating) {
            alert('Wait for opponent.');
            return;
        }

        if(this.currentGuess.indexOf(-1) >= 0) {
            alert('Missing pins in guess.');
            return;
        }

        alert('Guess submitted.. Wait for opponent.');

        RestClient.submit(Session.getToken(), Session.getExtra('GameID'), this.currentGuess, (res: HTTP.Response) => {
            if(res.code === 200) {
                this.update();
            } else {
                alert('Waiting for opponent.');
            }
        });

    };

    private update = (): void => {
        this.updating = true;        

        RestClient.update(Session.getToken(), Session.getExtra('GameID'), (res: HTTP.Response) => {
            if(res.code === 200) {
                let json = JSON.parse(res.text);
                let colors = []; 

                if(json.host.id === Session.getDecodedToken().uid) {
                    colors = json.host.turns[this.currentTurn].correctGuess;
                } else {
                    colors = json.guest.turns[this.currentTurn].correctGuess;
                }

                this.setPins(this.currentTurn, colors);

                this.boardTable.getRowElement(this.currentTurn).classList.remove(GameController.CSS_CURRENT_TURN);

                this.currentTurn++;
                this.currentGuess = [-1, -1, -1, -1];
                this.updating = false;

                this.boardTable.getRowElement(this.currentTurn).classList.add(GameController.CSS_CURRENT_TURN);
            } else if(res.code === 420) {
                this.update();
            } else {
                this.update();
                //alert('Other player left game.');
                //SPA.popToHome();
            }
        });
    };

    private setPins(turn: number, colors: number[]): void {
        for(let i = 0; i < colors.length; i++) {
            let pin = this.boardPins[turn][i];
            pin.classList.remove(GameController.CSS_UNSET_COLOR);

            let color = '';
            
            if(colors[i] === 2) {
                color = GameController.CSS_CORRECT_PLACE;
            } else if(colors[i] === 1) {
                color = GameController.CSS_CORRECT_COLOR;
            }

            pin.classList.add(color);
        }
    }

    private createOpponentPins(): void {
        let json = Session.getExtra('GameInfo');
        let spans: HTMLSpanElement[] = [];

        let text = document.createElement('span');
        text.textContent = 'Opponent pins: ';

        spans.push(text);

        for(let color of json.colorCodes) {
            let span = document.createElement('span');
            span.classList.add('badge', GameController.CSS_COLORS[color]);
            span.appendChild(document.createElement('br'));

            spans.push(span);
        }

        this.opponentPins.addRow(spans);
    }

    private createPalette(): void {
        for(let i = 0; i < GameController.CSS_COLORS.length; i++) {
            let cName = GameController.CSS_COLORS[i];
            let elem = document.createElement('a');
            elem.classList.add(cName, 'btn', 'btn-circle', 'btn-lg');

            let button = new Link(elem);
            button.onClick = (e) => {
                this.currentColor = i;
            };

            this.paletteButtons.push(button);
            this.paletteTable.addRow([button.getElement()]);
        }
    }

    private createBoard(): void {
        for(let i = 0; i < GameController.NUM_TURNS; i++) {
            let buttonRow: Link[] = [];
            let contentRow: (HTMLElement | string)[] = [];

            contentRow.push(i.toString());

            for(let j = 0; j < 4; j++) {
                let elem = document.createElement('a');
                elem.classList.add(GameController.CSS_UNSET_COLOR, 'btn', 'btn-circle', 'btn-lg');

                let button = new Link(elem);
                button.onClick = (e) => {
                    if(this.currentColor === -1 ||
                        this.currentTurn < 0 ||
                        this.currentTurn > GameController.NUM_TURNS ||
                        this.boardButtons[this.currentTurn].indexOf(button) < 0) {
                        return;
                    }

                    let classList = button.getElement().classList;
                    classList.remove.apply(classList, GameController.CSS_COLORS);
                    classList.remove(GameController.CSS_UNSET_COLOR);
                    classList.add(GameController.CSS_COLORS[this.currentColor]);

                    this.currentGuess[j] = this.currentColor;
                };

                buttonRow.push(button);
                contentRow.push(button.getElement());
            }

            contentRow.push(this.createPins());

            this.boardButtons.push(buttonRow);
            this.boardTable.addRow(contentRow);
        }

        this.boardTable.getRowElement(0).classList.add(GameController.CSS_CURRENT_TURN);
    }

    private createPins(): HTMLElement {
        let div: HTMLElement = document.createElement('div');
        let row: HTMLSpanElement[] = [];

        for(let i = 0; i < 4; i++) {
            let span = document.createElement('span');
            span.classList.add('badge', GameController.CSS_UNSET_COLOR);
            span.appendChild(document.createElement('br'));

            div.appendChild(span);
            row.push(span);
        }
 
        this.boardPins.push(row);

        return div;
    }

    public onActivated(): void {
        console.log('GameController onActivated()');

    }

    public onDestroyed(): void {

    }

    public clean(): void {
        this.paletteTable.dispose();
        this.opponentPins.dispose();
        this.boardTable.dispose();

        for(let button of this.paletteButtons) {
            button.dispose();
        }

        for(let row of this.boardButtons) {
            for(let button of row) {
                button.dispose();
            }
        }

        this.submitButton.dispose();
    }
}