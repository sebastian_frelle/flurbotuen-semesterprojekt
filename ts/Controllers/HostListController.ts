/// <reference path="Controller.ts" />
/// <reference path="../RestClient.ts" />
/// <reference path="../Controls/Table.ts" />
/// <reference path="../Controls/Link.ts" />
/// <reference path="LobbyController.ts" />

class HostListController extends Controller {
    private hostGame: Link;
    private refresh: Link;
    private hostedGames: Table;
    private gameLinks: Link[];

    constructor(container: HTMLElement) {
        super(container);

        this.hostGame = new Link(<HTMLAnchorElement>this.getElementByControlId('host'));
        this.hostGame.onClick = (e) => {
            RestClient.hostGame(Session.getToken(), (res: HTTP.Response) => {
                if(res.code === 200) {
                    let data = JSON.parse(res.text);
                    Session.setExtra('GameID', data.gameid);
                    
                    SPA.loadTemplate('templates/lobby.html', SPA.ViewType.Page, LobbyController);
                }
            });
        };

        this.refresh = new Link(<HTMLAnchorElement>this.getElementByControlId('refresh'));
        this.refresh.onClick = (e) => {
            this.onActivated();
        };

        this.hostedGames = new Table(<HTMLTableElement>this.getElementByControlId('game-table'));
        this.gameLinks = [];
    }

    public onActivated(): void {
        console.log('HostListController onActivated()');

        this.hostedGames.clearAddedRows();

        RestClient.getHostedGames(Session.getToken(), (res: HTTP.Response) => {
            if(res.code === 200) {
                let json = JSON.parse(res.text);

                for(let key in json) {
                    let link = new Link(document.createElement('a'));
                    link.getElement().textContent = key;
                    link.onClick = (e) => {
                        RestClient.joinGame(Session.getToken(), key, (joinRes: HTTP.Response) => {
                            if(joinRes.code === 200) {
                                Session.setExtra('GameID', key);
                                SPA.loadTemplate('templates/lobby.html', SPA.ViewType.Page, LobbyController);
                            } else {
                                alert('Could not join game. Code: ' + joinRes.code);
                            }
                        });
                    };

                    this.gameLinks.push(link);

                    RestClient.getUserName(Session.getToken(), json[key].hostid, (userRes: HTTP.Response) => {
                        if(userRes.code === 200) {
                            this.hostedGames.addRow([link.getElement(), userRes.text]);
                        } else {
                            alert('Code: ' + userRes.code + '\n' + userRes.text);
                        }
                    });
                }
            } else {
                alert('Code: ' + res.code + '\n' + res.text);
            }
        });
    }

    public clean(): void {
        this.hostGame.dispose();
        this.refresh.dispose();
        this.hostedGames.dispose();

        for(let link of this.gameLinks) {
            link.dispose();
        }
    }
}