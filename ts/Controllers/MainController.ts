/// <reference path="Controller.ts" />
/// <reference path="LoginController.ts" />
/// <reference path="RegisterController.ts" />
/// <reference path="UserInfoController.ts" />
/// <reference path="../Controls/Link.ts" />
/// <reference path="../Session.ts" />
/// <reference path="../Controls/Link.ts" />
/// <reference path="../Controls/Menu.ts" />
/// <reference path="HostListController.ts" />

class MainController extends Controller {
    private homeLink: Link;
    private mainMenu: Menu;

    private loginLink: Link;
    private registerLink: Link;

    private userLink: Link;
    private logoutLink: Link;

    constructor(container: HTMLElement) {
        super(container);

        this.homeLink = new Link(<HTMLAnchorElement>this.getElementByControlId('home-link'));

        this.homeLink.onClick = function(e) {
            SPA.popToHome();
        };

        this.mainMenu = new Menu(<HTMLUListElement>this.getElementByControlId('main-menu'));
    }

    public onActivated(): void {
        console.log('MainController onActivated()');
        
        if(!Session.isAuthenticated()) {
            if(!this.loginLink) {
                this.loginLink = new Link(document.createElement('a'));
                this.loginLink.getElement().textContent = 'Login';

                this.loginLink.onClick = function(e) {
                    if(!(SPA.getTopController() instanceof LoginController)) {
                        SPA.loadTemplate('templates/login.html', SPA.ViewType.Page, LoginController);
                    }
                };

                this.mainMenu.addMenuItem(this.loginLink);
            }

            if(!this.registerLink) {
                this.registerLink = new Link(document.createElement('a'));
                this.registerLink.getElement().textContent = 'Register';

                this.registerLink.onClick = function(e) {
                    if(!(SPA.getTopController() instanceof RegisterController)) {
                        SPA.loadTemplate('templates/register.html', SPA.ViewType.Page, RegisterController);
                    }
                };

                this.mainMenu.addMenuItem(this.registerLink);
            }
        } else {
            this.keepAlive();

            this.mainMenu.removeMenuItem(this.loginLink);
            this.mainMenu.removeMenuItem(this.registerLink);

            if(!this.userLink) {
                this.userLink = new Link(document.createElement('a'));
                
                RestClient.getUser(Session.getToken(), (res: HTTP.Response) => {
                    if(res.code === 200) {
                        let json = JSON.parse(res.text);
                        this.userLink.getElement().textContent = json.name;
                    } else {
                        alert('Code: ' + res.code + '\n' + res.text);
                    }
                });

                this.userLink.onClick = function(e) {
                    if(!(SPA.getTopController() instanceof UserInfoController)) {
                        SPA.loadTemplate('templates/userinfo.html', SPA.ViewType.Page, UserInfoController);
                    }
                }

                this.mainMenu.addMenuItem(this.userLink);
            }

            if(!this.logoutLink) {
                this.logoutLink = new Link(document.createElement('a'));
                this.logoutLink.getElement().textContent = 'Logout';

                this.logoutLink.onClick = function(e) {
                    Session.removeToken();
                    window.location.href = '/';
                };

                this.mainMenu.addMenuItem(this.logoutLink);
            }

            SPA.loadTemplate('templates/hostlist.html', SPA.ViewType.Page, HostListController);
        }
    }

    private keepAlive = (): void => {
        let timeout = setTimeout(this.keepAlive, 10000);

        RestClient.ping(Session.getToken(), function(res: HTTP.Response) {
            if(res.code !== 200) {
                clearTimeout(timeout);
                alert('Connection to server lost/session expired');
                Session.removeToken();
                window.location.href = '/';
            }
        });
    }

    public clean(): void {
        this.homeLink.dispose();
        this.loginLink.dispose();
        this.registerLink.dispose();
        this.userLink.dispose();
        this.logoutLink.dispose();
        this.mainMenu.dispose();
    }
}