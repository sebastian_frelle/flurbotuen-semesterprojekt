/// <reference path="Controller.ts" />
/// <reference path="../Controls/Link.ts" />
/// <reference path="../Controls/Input.ts" />
/// <reference path="../RestClient.ts" />

class RegisterController extends Controller {
    private email: Input;
    private password: Input;
    private registerLink: Link;

    constructor(container: HTMLElement) {
        super(container);

        let submit = (e: Event) => {
            RestClient.register({
                username: this.email.getValue(),
                password: this.password.getValue()
            }, function(res: HTTP.Response) {
                if(res.code === 200) {
                    alert('Successfully registered');
                    SPA.popToHome();
                } else {
                    alert('Code: ' + res.code + '\n' + res.text);
                }
            });
        };

        this.email = new Input(<HTMLInputElement>this.getElementByControlId('email'));
        this.password = new Input(<HTMLInputElement>this.getElementByControlId('password'));

        this.registerLink = new Link(<HTMLAnchorElement>this.getElementByControlId('register'));
        this.registerLink.getElement().textContent = 'Register';

        this.registerLink.onClick = submit;
        this.email.onEnter = submit;
        this.password.onEnter = submit;
    }

    public onActivated(): void {
        console.log('RegisterController onActivated()');
    }

    public clean(): void  {
        this.registerLink.dispose();
        this.email.dispose();
        this.password.dispose();
    }
}