/// <reference path="Controller.ts" />
/// <reference path="GameController.ts" />
/// <reference path="../Controls/Table.ts" />
/// <reference path="../RestClient.ts" />

class LobbyController extends Controller {
    private startGameLink: Link;
    private playerTable: Table;

    private prevJSON: any;
    private refreshTimeout: number;

    constructor(container: HTMLElement) {
        super(container);

        this.startGameLink = new Link(<HTMLAnchorElement>this.getElementByControlId('start'));
        this.startGameLink.hide();

        this.startGameLink.onClick = function(e) {
            RestClient.start(Session.getToken(), Session.getExtra('GameID'), function(res: HTTP.Response) {
                if(res.code == 200) {
                    Session.setExtra('GameInfo', JSON.parse(res.text));
                    SPA.popToHome();
                    SPA.loadTemplate('templates/game.html', SPA.ViewType.Page, GameController)
                } else {
                    alert('Cant start yet.');
                }
            });
        };

        this.playerTable = new Table(<HTMLTableElement>this.getElementByControlId('player-table'));

        this.refreshTimeout = 0;
    }

    public onActivated(): void {
        console.log('LobbyController onActivated');

        this.refreshPlayerlist();
    }

    private refreshPlayerlist = (): void => {
        this.refreshTimeout = setTimeout(this.refreshPlayerlist, 1000);

        RestClient.getHostedGame(Session.getToken(), Session.getExtra('GameID'), (res: HTTP.Response) => {            
            if(res.code === 200) {
                let json = JSON.parse(res.text);

                if(!this.prevJSON) {
                    this.prevJSON = {
                        hostid: '',
                        guestid: ''
                    }
                }

                if(this.prevJSON) {
                    if(json.hostid !== this.prevJSON.hostid && json.guestid !== this.prevJSON.guestid) {
                        if(json.hostid === Session.getDecodedToken().uid && json.guestid) {
                            this.startGameLink.show();
                        } else {
                            this.startGameLink.hide();
                            if(json.guestid === Session.getDecodedToken().uid) {
                                this.wait();
                            }
                        }

                        let addRow = (res2: HTTP.Response) => {
                            if(res2.code === 200) {
                                this.playerTable.addRow([res2.text]);
                            }
                        };

                        this.playerTable.clearAddedRows();

                        RestClient.getUserName(Session.getToken(), json.hostid, addRow);

                        if(json.guestid) {
                            RestClient.getUserName(Session.getToken(), json.guestid, addRow);
                        }
                    }
                }
            } else {
                console.log('1');//alert('Lost connection to lobby.');
                //SPA.popToHome();
            }
        });
    };

    private wait = (): void => {
        this.clearTimeout();
                
        RestClient.wait(Session.getToken(), Session.getExtra('GameID'), (res: HTTP.Response) => {
            if(res.code === 200) {
                Session.setExtra('GameInfo', JSON.parse(res.text));
                SPA.popToHome();
                SPA.loadTemplate('/templates/game.html', SPA.ViewType.Page, GameController);
            } else if(res.code === 404) {
                console.log('2');//alert('Lost connection to lobby.');
                //SPA.popToHome();
            } else if(res.code === 420) {
                this.wait();
            }
        });
    };

    private clearTimeout(): void {
        if(this.refreshTimeout !== 0) {
            clearTimeout(this.refreshTimeout);
            this.refreshTimeout = 0;
        }
    }

    public onDestroyed(): void {
        this.clearTimeout();

        RestClient.leave(Session.getToken(), Session.getExtra('GameID'), function(res: HTTP.Response) {

        });
    }

    public clean(): void {
        this.startGameLink.dispose();
        this.playerTable.dispose();
    }
}