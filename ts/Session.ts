namespace Session {
    interface Token {
        uid: string
    }

    let decodedToken: Token;
    let extra: {[key: string]: any} = {};

    export function setToken(token: string): void {
        sessionStorage.setItem('token', token);
    }

    export function getToken(): string {
        return sessionStorage.getItem('token');
    }

    export function removeToken(): void {
        sessionStorage.removeItem('token');
        decodedToken = null;
    }

    export function isAuthenticated(): boolean {
        return sessionStorage.getItem('token') !== null;
    }

    export function getDecodedToken(): Token {
        if(!decodedToken) {
            let tmp = JSON.parse(atob(getToken().split('.')[1]));
            decodedToken = {
                uid: tmp.uid
            };
        }

        return decodedToken;
    };

    export function setExtra(name: string, value: any): void {
        extra[name] = value;
    }

    export function getExtra(name: string): any {
        return extra[name];
    }
}