/// <reference path="SPA.ts" />
/// <reference path="Controllers/MainController.ts" />


document.addEventListener('DOMContentLoaded', function() {
    App.init();
});

namespace App {
    export function init(): void {
        SPA.init(document.getElementById('spa-container'), MainController);
    }
}