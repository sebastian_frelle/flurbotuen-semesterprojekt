/// <reference path="HTTP.ts" />
/// <reference path="Controllers/Controller.ts" />

namespace SPA {
    export enum ViewType {
        Page = 1,
        Modal
    }

    let container: HTMLElement;
    let viewContainer: HTMLElement;
    let views: HTMLElement[];
    let mainController: Controller;
    let controllers: Controller[];

    export function init(containerElement: HTMLElement, controller: typeof Controller): void {
        container = containerElement;
        container.classList.add('spa-container');
        container.style.height = (window.innerHeight - container.offsetTop) + 'px';

        window.addEventListener('resize', function(e) {
            container.style.height = (window.innerHeight - container.offsetTop) + 'px';
        });

        viewContainer = document.createElement('div');
        viewContainer.classList.add('spa-view-container');
        containerElement.appendChild(viewContainer);

        mainController = new controller(container);
        mainController.onActivated();

        views = [];
        controllers = [];

        window.onpopstate = function(e: Event) {
            if(views.length == 0) {
                window.history.back();
                return false;
            }

            popView();
        };
    }

    export function loadTemplate(url: string, viewType: ViewType, controller: typeof Controller): void {
        if(viewType === ViewType.Modal) {
            throw new Error('ViewType Modal not implemented.');
        }
      
        HTTP.get(url, [], function(res: HTTP.Response): void {
            if(res.code === 200) {
                if(!controller) {
                    throw new Error('You must attach a controller.');
                }

                let newView: HTMLElement = pushView(res.text, viewType);
                controllers.push(new controller(newView));
                controllers[controllers.length - 1].onActivated();

                window.history.pushState({}, url);
            } else {
                throw new Error('Something went wrong loading tempalte at url: ' + url);
            }
        });
    }

    function pushView(html: string, viewType: ViewType): HTMLElement {
        let element: HTMLElement = document.createElement('div');

        if(viewType === ViewType.Page) {
            element.classList.add('view');
            element.innerHTML = html;
        } else {
            element.classList.add('view-modal');

            element.addEventListener('click', function(e: Event) { 
                if(e.target === element) {
                    window.history.back();
                }
            });

            let contentElement: HTMLElement = document.createElement('div');
            contentElement.classList.add('view-modal-content');
            contentElement.innerHTML = html;

            element.appendChild(contentElement);
        }

        if(views.length > 0) {
            viewContainer.removeChild(views[views.length - 1]);
        }

        viewContainer.appendChild(element);
        views.push(element);

        return element;
    }

    export function popView(): void {
        let element: HTMLElement = views.pop();
        viewContainer.removeChild(element);

        if(views.length > 0) {
            viewContainer.appendChild(views[views.length - 1]);
        }

        controllers[controllers.length - 1].onDestroyed();
        controllers[controllers.length - 1].clean();
        controllers.pop();

        if(controllers.length === 0) {
            mainController.onActivated();
        } else {
            controllers[controllers.length - 1].onActivated();
        }
    }

    export function popToHome(): void {
        while(views.length > 0) {
            popView();
        }
    }

    export function getTopController(): Controller {
        if(controllers.length === 0) {
            return mainController;
        }

        return controllers[controllers.length - 1];
    }
}