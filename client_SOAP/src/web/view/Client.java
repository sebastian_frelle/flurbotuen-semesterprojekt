package web.view;

import java.net.URL;
import java.util.Scanner;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import web.view.dto.components.Status;
import web.view.dto.request.*;
import web.view.dto.response.*;


public class Client {
	
	static IAuthService iAuthService;
	static Scanner scanner = new Scanner (System.in);
	static String token;
	static boolean isAdmin = false; 
	
	
	public static void main(String[] args) {
		connect();
		boolean wantToContinue = true;
		
		do {
			printWelcomeMsg();
			String option = scanner.nextLine();
			
			switch (option) {
			case "1":
				register();
				break;
			case "2":
				if (login()) {
					boolean loggedIn = true;
					do {
						printLoggedInOptions();			
						option = scanner.nextLine();
						
						switch (option) {
						case "1":
							changePassword();
							break;
						case "2":
							getUsers();
							break;
						case "3":
							deleteUser();
							break;
						case "4":
							createUser();
							break;
						case "5":
							changeUserPassword();
							break;
						case "6":
							setAdmin();
							break;
						case "0":
							loggedIn = false;
							break;
						default:
							System.out.println("Invalid option");
							break;
						}
					} while (loggedIn);
				}
				break;
			case "0":
				wantToContinue = false;
				break;
			default:
				System.out.println("Invalid option");
				break;
			}
		} while (wantToContinue);
	}


	private static void printWelcomeMsg() {
		System.out.println(iAuthService.greet(new GreetDto()).getMsg());
		System.out.println("Choose one of the following options:");
		System.out.println("1: Register");
		System.out.println("2: Login");
		System.out.println("0: Close");
	}

	private static void printLoggedInOptions() {
		System.out.println("Choose one of the following options:");
		System.out.println("1: Change password");
		System.out.println("2: See all users");

		if(isAdmin) {
			System.out.println("3: Delete user");
			System.out.println("4: Create user");
			System.out.println("5: Change password for a user");
			System.out.println("6: Change admin status for a user");
		}
		System.out.println("0: Log out");
	}

	private static void connect() {
		try {
			URL url = new URL("http://ubuntu4.javabog.dk:9071/auth?wsdl");
			QName qname = new QName("http://impl.view.web/", "AuthServiceService");
			Service service = Service.create(url, qname);
			iAuthService = service.getPort(IAuthService.class);
		} catch (Exception e) {
			e.printStackTrace();
		}   
		
	}
	
	private static void register() {
		System.out.println("Type your username");
		String username = scanner.nextLine();
		System.out.println("Type your password");
		String password = scanner.nextLine();
		
		RegisterDto registerDto = new RegisterDto(username, password);
		System.out.println(iAuthService.register(registerDto).getMsg() + "\n");
	}
	
	private static boolean login() {
		System.out.println("Type your username");
		String username = scanner.nextLine();
		System.out.println("Type your password");
		String password = scanner.nextLine();
		
		LoginDto loginDto = new LoginDto(username, password);
		LoginStatusDto loginStatusDto = iAuthService.login(loginDto);
		System.out.println(loginStatusDto.getMsg() + "\n");
		if (loginStatusDto.getStatus() == Status.SUCCESS) {
			token = loginStatusDto.getJwt();
			isAdmin = iAuthService.getUser(token, new GetUserDto(null, username)).getUser().getAdmin();
			return true;
		}
		else
			return false;
	}
	
	private static void changePassword() {
		System.out.println("Type your old password");
		String oldPassword = scanner.nextLine();
		System.out.println("Type your new password");
		String newPassword = scanner.nextLine();
		System.out.println("Type your new password again");
		String newPasswordRetype = scanner.nextLine();
		
		ChangePasswordDto changePasswordDto = new ChangePasswordDto(oldPassword, newPassword, newPasswordRetype);	
		System.out.println(iAuthService.changePassword(token, changePasswordDto).getMsg()+ "\n");
	}
	
	private static void getUsers() {
		GetUsersResponseDto usersResponseDto = iAuthService.getUsers(token);
		System.out.println(usersResponseDto.getMsg());
		
		for (UserResponseDto user : usersResponseDto.getUsers()) {
			System.out.println("Id: " + user.getId() + "\tusername: " + user.getUsername());
		}
		System.out.println("");
	}
	
	private static void deleteUser() {
		if (isAdmin) {
			System.out.println("Type the id of the user you want to delete");
			String id = scanner.nextLine();
			System.out.println(iAuthService.adminDeleteUser(token, Long.parseLong(id)).getMsg());
		}
		else{
			System.out.println("Invalid option");
		}
		System.out.println("");
	}
	
	private static void createUser() {
		if (isAdmin) {
			System.out.println("Type username for new user");
			String username = scanner.nextLine();
			System.out.println("Type password for new user");
			String password = scanner.nextLine();
			System.out.println("Type y if you want the new user to be admin");
			String admin = scanner.nextLine();
			
			AdminCreateUserDto adminCreateUserDto = new AdminCreateUserDto(username, password, admin.equals("y"));
			System.out.println(iAuthService.adminCreateUser(token, adminCreateUserDto).getMsg());
		}
		else{
			System.out.println("Invalid option");
		}
		System.out.println("");
	}
	
	private static void changeUserPassword() {
		if (isAdmin) {
			System.out.println("Type user id");
			String id = scanner.nextLine();
			System.out.println("Type the old user password");
			String oldPassword = scanner.nextLine();
			System.out.println("Type the new user password");
			String newPassword = scanner.nextLine();
			System.out.println("Type the new user password again");
			String newPasswordRetype = scanner.nextLine();
			
			AdminChangePasswordDto adminChangePasswordDto = new AdminChangePasswordDto(Long.parseLong(id), oldPassword, newPassword, newPasswordRetype);
			System.out.println(iAuthService.adminChangePassword(token, adminChangePasswordDto).getMsg());
		}
		else{
			System.out.println("Invalid option");
		}
		System.out.println("");
	}
	
	private static void setAdmin() {
		if (isAdmin) {
			System.out.println("Type user id");
			String id = scanner.nextLine();
			System.out.println("Type y if you want the user to be admin, and n if not");
			String admin = scanner.nextLine();
			
			AdminSetAdminDto adminSetAdminDto = new AdminSetAdminDto(Long.parseLong(id), admin.equals("y"));
			System.out.println(iAuthService.adminSetAdmin(token, adminSetAdminDto).getMsg());
		}
		else{
			System.out.println("Invalid option");
		}
		System.out.println("");
	}	
}