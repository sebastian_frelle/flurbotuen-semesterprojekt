package web.view.dto.request;

import javax.xml.bind.annotation.*;

@XmlRootElement
@XmlType
@XmlAccessorType(XmlAccessType.FIELD)
public class AdminSetAdminDto {
    @XmlElement(required = true)
    private Long id;

    @XmlElement(required = true)
    private Boolean admin;

    public AdminSetAdminDto() {}

    public AdminSetAdminDto(Long id, Boolean admin) {
        this.id = id;
        this.admin = admin;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getAdmin() {
        return admin;
    }

    public void setAdmin(Boolean admin) {
        this.admin = admin;
    }
}
