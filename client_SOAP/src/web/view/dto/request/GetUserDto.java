package web.view.dto.request;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "userdata")
@XmlType(name = "something")
@XmlAccessorType(XmlAccessType.FIELD)
public class GetUserDto {
    @XmlElement(name = "id", nillable = true)
    private Long id;

    @XmlElement(name = "username", nillable = true)
    private String username;

    public GetUserDto() {}

    public GetUserDto(Long id, String username) {
        this.id = id;
        this.username = username;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
