package web.view.dto.request;

import javax.xml.bind.annotation.*;

@XmlRootElement
@XmlType
@XmlAccessorType(XmlAccessType.FIELD)
public class AdminChangePasswordDto {
    @XmlElement(required = true)
    private Long id;

    @XmlElement(required = true)
    private String oldPassword;

    @XmlElement(required = true)
    private String newPassword;

    @XmlElement(required = true)
    private String newPasswordRetype;

    public AdminChangePasswordDto() {}

    public AdminChangePasswordDto(Long id, String oldPassword,
                                  String newPassword, String newPasswordRetype) {
        this.id = id;
        this.oldPassword = oldPassword;
        this.newPassword = newPassword;
        this.newPasswordRetype = newPasswordRetype;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getNewPasswordRetype() {
        return newPasswordRetype;
    }

    public void setNewPasswordRetype(String newPasswordRetype) {
        this.newPasswordRetype = newPasswordRetype;
    }
}
