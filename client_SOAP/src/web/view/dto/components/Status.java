package web.view.dto.components;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "status")
public enum Status {
    SUCCESS("success"), FAIL("fail");

    private String status;

    Status(String status) {
        this.status = status;
    }
}
