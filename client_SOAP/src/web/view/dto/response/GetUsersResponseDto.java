package web.view.dto.response;

import javax.xml.bind.annotation.*;

import web.view.dto.components.Status;

import java.util.List;

@XmlRootElement(name = "usersData")
@XmlAccessorType(XmlAccessType.FIELD)
public class GetUsersResponseDto extends BasicStatusDto {
    @XmlElementWrapper
    @XmlElement(name = "user")
    private List<UserResponseDto> users;

    public GetUsersResponseDto() {}

    public GetUsersResponseDto(Status status, String msg) {
        super(status, msg);
    }

    public GetUsersResponseDto(List<UserResponseDto> users) {
        this.users = users;
    }

    public GetUsersResponseDto(Status status, String msg, List<UserResponseDto> users) {
        super(status, msg);
        this.users = users;
    }

    public List<UserResponseDto> getUsers() {
        return users;
    }

    public void setUsers(List<UserResponseDto> users) {
        this.users = users;
    }
}
