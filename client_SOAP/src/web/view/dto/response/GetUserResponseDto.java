package web.view.dto.response;

import javax.xml.bind.annotation.XmlRootElement;

import web.view.dto.components.Status;

@XmlRootElement(name = "userData")
public class GetUserResponseDto extends BasicStatusDto {
    private UserResponseDto user;

    public GetUserResponseDto() {}

    public GetUserResponseDto(Status status, String msg) {
        super(status, msg);
    }

    public GetUserResponseDto(UserResponseDto user) {
        this.user = user;
    }

    public GetUserResponseDto(Status status, String msg, UserResponseDto user) {
        super(status, msg);
        this.user = user;
    }

    public UserResponseDto getUser() {
        return user;
    }

    public void setUser(UserResponseDto user) {
        this.user = user;
    }
}
