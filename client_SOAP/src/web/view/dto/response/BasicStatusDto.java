package web.view.dto.response;

import javax.xml.bind.annotation.XmlRootElement;

import web.view.dto.components.Status;

@XmlRootElement(name = "status")
public class BasicStatusDto {
    private Status status;
    private String msg;

    public BasicStatusDto() {}

    public BasicStatusDto(Status status, String msg) {
        this.status = status;
        this.msg = msg;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
