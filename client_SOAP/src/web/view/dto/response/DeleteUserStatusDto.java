package web.view.dto.response;

import javax.xml.bind.annotation.XmlRootElement;

import web.view.dto.components.Status;

@XmlRootElement(name = "deleteUserStatus")
public class DeleteUserStatusDto extends BasicStatusDto {
    private Long id;

    public DeleteUserStatusDto() {
    }

    public DeleteUserStatusDto(Status status, String msg) {
        super(status, msg);
    }

    public DeleteUserStatusDto(Long id) {
        this.id = id;
    }

    public DeleteUserStatusDto(Status status, String msg, Long id) {
        super(status, msg);
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
