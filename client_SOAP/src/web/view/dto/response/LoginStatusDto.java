package web.view.dto.response;

import javax.xml.bind.annotation.XmlRootElement;

import web.view.dto.components.Status;

@XmlRootElement(name = "loginstatus")
public class LoginStatusDto extends BasicStatusDto {
    private String jwt;

    public LoginStatusDto() {}

    public LoginStatusDto(Status status, String msg) {
        super(status, msg);
    }

    public LoginStatusDto(Status status, String msg, String jwt) {
        super(status, msg);
        this.jwt = jwt;
    }

    public String getJwt() {
        return jwt;
    }

    public void setJwt(String jwt) {
        this.jwt = jwt;
    }
}
