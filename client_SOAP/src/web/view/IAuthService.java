package web.view;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlElement;

import web.view.dto.request.*;
import web.view.dto.response.*;

@WebService
public interface IAuthService {
    @WebMethod
    BasicStatusDto greet(
            @WebParam(name = "greetData")
            @XmlElement(required = true) GreetDto greetDto);

    @WebMethod
    LoginStatusDto login(
            @WebParam(name = "loginData")
            @XmlElement(required = true) LoginDto loginDto);

    @WebMethod
    BasicStatusDto register(
            @WebParam(name = "registerData")
            @XmlElement(required = true) RegisterDto registerDto);

    @WebMethod
    BasicStatusDto changePassword(
            @WebParam(name = "x-access-token", header = true) String token,

            @WebParam(name = "changePasswordData")
            @XmlElement(required = true) ChangePasswordDto changePasswordDto
    );

    @WebMethod
    GetUserResponseDto getUser(
            @WebParam(name = "x-access-token", header = true) String token,

            @WebParam(name = "getUserData")
            @XmlElement(required = true) GetUserDto getUserDto
    );

    @WebMethod
    GetUsersResponseDto getUsers(
            @WebParam(name = "x-access-token", header = true) String token
    );

    @WebMethod
    BasicStatusDto adminDeleteUser(
            @WebParam(name = "x-access-token", header = true) String token,

            @WebParam(name = "userId")
            @XmlElement(required = true) Long id
    );

    @WebMethod
    BasicStatusDto adminCreateUser(
            @WebParam(name = "x-access-token", header = true) String token,

            @WebParam(name = "adminCreateUserData")
            @XmlElement(required = true) AdminCreateUserDto adminCreateUserDto
    );

    @WebMethod
    BasicStatusDto adminChangePassword(
            @WebParam(name = "x-access-token", header = true) String token,

            @WebParam(name = "adminChangePasswordData")
            @XmlElement(required = true) AdminChangePasswordDto adminChangePasswordDto
    );

    @WebMethod
    BasicStatusDto adminSetAdmin(
            @WebParam(name = "x-access-token", header = true) String token,

            @WebParam(name = "adminSetAdminData")
            @XmlElement(required = true) AdminSetAdminDto adminSetAdminDto
    );
}
