import requests
import json
import getpass

headers = {'content-type': 'application/json', 'x-access-token': ''}
admin = 0

def menuAdmin( answer ):
    if(answer == '1'):
        listUsers()
    elif(answer == '2'):
        deleteUser()
    elif(answer == '3'):
        createUser()
    elif(answer == '4'):
        changePassWord()
    elif(answer == '5'):
        makeAdmin()
    elif(answer == '6'):
        newPassword()
    elif(answer == '7'):
        return
    else:
        print('You answer is not an option')
        loggedIn(admin)    

def menuNotAdmin( answer ):
    if(answer == '1'):
        newPassword()
    elif(answer == '2'):
        listUsers()
    elif(answer == '3'):
        return
    else:
        print('You answer is not an option')
        loggedIn(admin)

def newPassword():
    old_password = input('Old password: ')
    new_password = input('New password: ')
    repeat = input('Repeat new password: ')

    url = 'http://ubuntu4.javabog.dk:1440/change_password'
    r = requests.post(url, data = json.dumps({'old_password' : old_password, 'new_password' : new_password, 'new_password_repeat' : repeat}), headers = headers)

    data = r.json()

    if(r.ok):
        print(data['msg'])
        print('Back to the menu: ')
        return loggedIn(admin)
    else:
        print(data['msg'])
        answer = input('Do you want to try again? type 1 of you do: ')
        if(answer == '1'):
            return newPassword()
        else:
            return loggedIn(admin)

def listUsers():
    url = 'http://ubuntu4.javabog.dk:1440/users'
    r = requests.get(url, headers = headers)
    data = r.json()
    if(r.ok):
        for x in range(0,len(data['users'])):
            print(data['users'][x])
        
        #SKRIV DET PÆNERE
        print('Back to the menu: ')
        return loggedIn(admin)
    else:
        print(data['msg'])
        answer = input('Do you want to try again? type 1 of you do: ')
        if(answer == '1'):
            return listUsers()
        else:
            return loggedIn(admin)


def deleteUser():
    id = input('User ID for the user you want to delete: ')
    url = 'http://ubuntu4.javabog.dk:1440/admin/user/' + id
    r = requests.delete(url, headers=headers)
    data = r.json()
    if(r.ok):
        print(data['msg'])
        print('Back to the menu: ')
        return loggedIn(admin)
    else:
        print(data['msg'])
        answer = input('Do you want to try again? type 1 of you do: ')
        if(answer == '1'):
            return deleteUser()
        else:
            return loggedIn(admin)

def createUser():
    username = input('Type username for new user: ')
    password = input('Type password for new user: ')
    adminBool = input('Should the new user be admin? true or false: ')
    if(adminBool =='true'):
        boolAdmin = True
    elif(adminBool == 'false'):
        boolAdmin = False
    else:
        return createUser()    

    url = 'http://ubuntu4.javabog.dk:1440/admin/create_user'
    r = requests.post(url, data = json.dumps({'username' : username, 'password' : password, 'admin' : boolAdmin}), headers=headers)

    data = r.json()
    if(r.ok):
        print(data['msg'])
        print('Back to the menu: ')
        return loggedIn(admin)
    else:
        print(data['msg'])
        answer = input('Do you want to try again? type 1 of you do: ')
        if(answer == '1'):
            return createUser()
        else:
            return loggedIn(admin)

def changePassword():
    id = input('User ID for the user you want to change password: ')
    old_password = input('Old password: ')
    new_password = input('New password: ')
    repeat = input('Repeat new password: ')

    url = 'http://ubuntu4.javabog.dk:1440/admin/change_password' + id
    r = requests.post(url, data = json.dumps({'old_password' : old_password, 'new_password' : new_password, 'new_password_repeat' : repeat}), headers = headers)
    
    data = r.json()
    if(r.ok):
        print(data['msg'])
        print('Back to the menu: ')
        return loggedIn(admin)
    else:
        print(data['msg'])
        answer = input('Do you want to try again? type 1 of you do: ')
        if(answer == '1'):
            return changePassword()
        else:
            return loggedIn(admin)

def makeAdmin():
    id = input('User ID for the user you want to change admin rights: ')
    adminBool = input('Should the user be admin? true or false: ')
    if(adminBool == 'true'):
        boolAdmin=True
    elif(adminBool == 'false'):
        boolAdmin = False
    else:
        print('Admin rogths should be true or false, try again.')
        return makeAdmin()

    url = 'http://ubuntu4.javabog.dk:1440/admin/set_admin/' + id
    r = requests.post(url, data = json.dumps({'admin' : boolAdmin}), headers=headers)
    data = r.json()
    if(r.ok):
        print(data['msg'])
        print('Back to the menu: ')
        return loggedIn(admin)
    else:
        print(data['msg'])
        answer = input('Do you want to try again? type 1 of you do: ')
        if(answer == '1'):
            return makeAdmin()
        else:
            return loggedIn(admin)

def loggedIn( admin ):
    if(admin == 1):
        print('You are admin, you now have the following options: \nType 1 for see a list of users \nType 2 to delete an user \nType 3 to create an user \nType 4 to change a password \nType 5 to change admin status of an user \nType 6 to change your own password \nType 7 to quit')  
        answer = input('Your choice: ')
        menuAdmin( answer )
    elif(admin == 0):
        print('You now have the following options: \nType 1 to create a new password \nType 2 to see a list of users \nType 3 to quit')
        answer = input('Your choice: ')
        menuNotAdmin( answer )




url = 'http://ubuntu4.javabog.dk:1440/'

r = requests.get(url)

data = r.json()

print(data['msg'])

notLoggedIn = True

while(notLoggedIn):

    username = input("Username: ")
    password = getpass.getpass("Password: ")
        
    url = 'http://ubuntu4.javabog.dk:1440/login'

    r = requests.post(url, data = json.dumps({'username' : username, 'password' : password}), headers=headers)

    if(r.ok):
        data = r.json()
        headers = {'content-type': 'application/json', 'x-access-token': data['jwt']}

        print('Welcome ' + username)

        url = 'http://ubuntu4.javabog.dk:1440/user'
        r = requests.get(url, params = {'username' : username}, headers=headers)
        
        if(r.ok):
            notLoggedIn = False
            data = r.json()
            admin = data['user']['admin']
            loggedIn( admin )

    elif(r.status_code == 404):
        print('Username not recognized, type 1 if you want to create an user')
        answer = input("Create new user? ")
        if(answer == '1'):
            url = 'http://ubuntu4.javabog.dk:1440/register'
            r = requests.post(url, data = json.dumps({'username' : username, 'password' : password}), headers=headers)
            
            if(r.ok):
                url = 'http://ubuntu4.javabog.dk:1440/login'
                r = requests.post(url, data = json.dumps({'username' : username, 'password' : password}), headers=headers)
                if(r.ok):
                    data = r.json()
                    headers = {'content-type': 'application/json', 'x-access-token': data['jwt']}
                    
                    url = 'http://ubuntu4.javabog.dk:1440/user'
                    r = requests.get(url, params = {'username' : username}, headers=headers)
        
                    if(r.ok):
                        data = r.json()
                        admin = data['user']['admin']
                        loggedIn( admin )
            else:
                print('Try to login again')
        else:
            print('Then try to login again')       
    else:
        data = r.json()
        print(data['msg'])
        print('Try to login again')

