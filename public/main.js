var HTTP;
(function (HTTP) {
    function get(url, headers, callback) {
        var req = new XMLHttpRequest();
        req.open('get', url, true);
        addHeaders(req, headers);
        req.send();
        req.onreadystatechange = function () {
            if (req.readyState === XMLHttpRequest.DONE) {
                var res = {
                    code: req.status,
                    text: req.responseText
                };
                callback(res);
            }
        };
    }
    HTTP.get = get;
    function post(url, param, headers, callback) {
        var req = new XMLHttpRequest();
        req.open('post', url, true);
        req.setRequestHeader('Content-Type', 'application/json');
        addHeaders(req, headers);
        if (param) {
            req.send(JSON.stringify(param));
        }
        else {
            req.send();
        }
        req.onreadystatechange = function () {
            if (req.readyState === XMLHttpRequest.DONE) {
                var res = {
                    code: req.status,
                    text: req.responseText
                };
                callback(res);
            }
        };
    }
    HTTP.post = post;
    function addHeaders(req, headers) {
        for (var _i = 0, headers_1 = headers; _i < headers_1.length; _i++) {
            var header = headers_1[_i];
            req.setRequestHeader(header.name, header.value);
        }
    }
})(HTTP || (HTTP = {}));
/// <reference path="HTTP.ts" />
var RestClient;
(function (RestClient) {
    var url = location.protocol + '//' + location.hostname + (location.port ? ':' + location.port : '') + '/api';
    function register(param, onDone) {
        HTTP.post(url + '/register', param, [], onDone);
    }
    RestClient.register = register;
    function login(param, onDone) {
        HTTP.post(url + '/login', param, [], onDone);
    }
    RestClient.login = login;
    function getUser(token, onDone) {
        HTTP.get(url + '/getUser', [{ name: 'x-access-token', value: token }], onDone);
    }
    RestClient.getUser = getUser;
    function hostGame(token, onDone) {
        HTTP.get(url + '/host', [{ name: 'x-access-token', value: token }], onDone);
    }
    RestClient.hostGame = hostGame;
    function joinGame(token, gameid, onDone) {
        HTTP.post(url + '/join', { gameid: gameid }, [{ name: 'x-access-token', value: token }], onDone);
    }
    RestClient.joinGame = joinGame;
    function start(token, gameid, onDone) {
        HTTP.post(url + '/start', { gameid: gameid }, [{ name: 'x-access-token', value: token }], onDone);
    }
    RestClient.start = start;
    function wait(token, gameid, onDone) {
        HTTP.post(url + '/wait', { gameid: gameid }, [{ name: 'x-access-token', value: token }], onDone);
    }
    RestClient.wait = wait;
    function submit(token, gameid, guess, onDone) {
        HTTP.post(url + '/submit', { gameid: gameid, guess: guess }, [{ name: 'x-access-token', value: token }], onDone);
    }
    RestClient.submit = submit;
    function update(token, gameid, onDone) {
        HTTP.post(url + '/update', { gameid: gameid }, [{ name: 'x-access-token', value: token }], onDone);
    }
    RestClient.update = update;
    function leave(token, gameid, onDone) {
        HTTP.post(url + '/leave', { gameid: gameid }, [{ name: 'x-access-token', value: token }], onDone);
    }
    RestClient.leave = leave;
    function getHostedGames(token, onDone) {
        HTTP.get(url + '/hostlist', [{ name: 'x-access-token', value: token }], onDone);
    }
    RestClient.getHostedGames = getHostedGames;
    function getHostedGame(token, gameid, onDone) {
        HTTP.get(url + '/getHostedGame/' + gameid, [{ name: 'x-access-token', value: token }], onDone);
    }
    RestClient.getHostedGame = getHostedGame;
    function getUserName(token, uid, onDone) {
        HTTP.get(url + '/getUserName/' + uid, [{ name: 'x-access-token', value: token }], onDone);
    }
    RestClient.getUserName = getUserName;
    function ping(token, onDone) {
        HTTP.get(url + '/ping', [{ name: 'x-access-token', value: token }], onDone);
    }
    RestClient.ping = ping;
})(RestClient || (RestClient = {}));
var Controller = (function () {
    function Controller(container) {
        if (this.constructor === Controller) {
            throw new Error('Controller cannot be directly instanciated.');
        }
        this.container = container;
        this.controlElements = {};
        this.parseContainer(this.container);
    }
    Controller.prototype.parseContainer = function (element) {
        if (element.classList.contains('control')) {
            var cid = element.getAttribute('data-cid');
            if (!cid) {
                throw new Error(element.outerHTML + "\nElement has no data-cid attribute.");
            }
            this.controlElements[cid] = element;
        }
        for (var i = 0; i < element.children.length; i++) {
            var child = element.children[i];
            this.parseContainer(child);
        }
    };
    Controller.prototype.getContainer = function () {
        return this.container;
    };
    Controller.prototype.getElementByControlId = function (controlId) {
        return this.controlElements[controlId];
    };
    Controller.prototype.onActivated = function () { };
    Controller.prototype.onDestroyed = function () { };
    Controller.prototype.clean = function () { };
    return Controller;
}());
/// <reference path="HTTP.ts" />
/// <reference path="Controllers/Controller.ts" />
var SPA;
(function (SPA) {
    var ViewType;
    (function (ViewType) {
        ViewType[ViewType["Page"] = 1] = "Page";
        ViewType[ViewType["Modal"] = 2] = "Modal";
    })(ViewType = SPA.ViewType || (SPA.ViewType = {}));
    var container;
    var viewContainer;
    var views;
    var mainController;
    var controllers;
    function init(containerElement, controller) {
        container = containerElement;
        container.classList.add('spa-container');
        container.style.height = (window.innerHeight - container.offsetTop) + 'px';
        window.addEventListener('resize', function (e) {
            container.style.height = (window.innerHeight - container.offsetTop) + 'px';
        });
        viewContainer = document.createElement('div');
        viewContainer.classList.add('spa-view-container');
        containerElement.appendChild(viewContainer);
        mainController = new controller(container);
        mainController.onActivated();
        views = [];
        controllers = [];
        window.onpopstate = function (e) {
            if (views.length == 0) {
                window.history.back();
                return false;
            }
            popView();
        };
    }
    SPA.init = init;
    function loadTemplate(url, viewType, controller) {
        if (viewType === ViewType.Modal) {
            throw new Error('ViewType Modal not implemented.');
        }
        HTTP.get(url, [], function (res) {
            if (res.code === 200) {
                if (!controller) {
                    throw new Error('You must attach a controller.');
                }
                var newView = pushView(res.text, viewType);
                controllers.push(new controller(newView));
                controllers[controllers.length - 1].onActivated();
                window.history.pushState({}, url);
            }
            else {
                throw new Error('Something went wrong loading tempalte at url: ' + url);
            }
        });
    }
    SPA.loadTemplate = loadTemplate;
    function pushView(html, viewType) {
        var element = document.createElement('div');
        if (viewType === ViewType.Page) {
            element.classList.add('view');
            element.innerHTML = html;
        }
        else {
            element.classList.add('view-modal');
            element.addEventListener('click', function (e) {
                if (e.target === element) {
                    window.history.back();
                }
            });
            var contentElement = document.createElement('div');
            contentElement.classList.add('view-modal-content');
            contentElement.innerHTML = html;
            element.appendChild(contentElement);
        }
        if (views.length > 0) {
            viewContainer.removeChild(views[views.length - 1]);
        }
        viewContainer.appendChild(element);
        views.push(element);
        return element;
    }
    function popView() {
        var element = views.pop();
        viewContainer.removeChild(element);
        if (views.length > 0) {
            viewContainer.appendChild(views[views.length - 1]);
        }
        controllers[controllers.length - 1].onDestroyed();
        controllers[controllers.length - 1].clean();
        controllers.pop();
        if (controllers.length === 0) {
            mainController.onActivated();
        }
        else {
            controllers[controllers.length - 1].onActivated();
        }
    }
    SPA.popView = popView;
    function popToHome() {
        while (views.length > 0) {
            popView();
        }
    }
    SPA.popToHome = popToHome;
    function getTopController() {
        if (controllers.length === 0) {
            return mainController;
        }
        return controllers[controllers.length - 1];
    }
    SPA.getTopController = getTopController;
})(SPA || (SPA = {}));
var Session;
(function (Session) {
    var decodedToken;
    var extra = {};
    function setToken(token) {
        sessionStorage.setItem('token', token);
    }
    Session.setToken = setToken;
    function getToken() {
        return sessionStorage.getItem('token');
    }
    Session.getToken = getToken;
    function removeToken() {
        sessionStorage.removeItem('token');
        decodedToken = null;
    }
    Session.removeToken = removeToken;
    function isAuthenticated() {
        return sessionStorage.getItem('token') !== null;
    }
    Session.isAuthenticated = isAuthenticated;
    function getDecodedToken() {
        if (!decodedToken) {
            var tmp = JSON.parse(atob(getToken().split('.')[1]));
            decodedToken = {
                uid: tmp.uid
            };
        }
        return decodedToken;
    }
    Session.getDecodedToken = getDecodedToken;
    ;
    function setExtra(name, value) {
        extra[name] = value;
    }
    Session.setExtra = setExtra;
    function getExtra(name) {
        return extra[name];
    }
    Session.getExtra = getExtra;
})(Session || (Session = {}));
var Control = (function () {
    function Control() {
    }
    Control.prototype.show = function () {
        this.getElement().classList.remove('hidden');
    };
    Control.prototype.hide = function () {
        this.getElement().classList.add('hidden');
    };
    return Control;
}());
/// <reference path="Control.ts" />
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Input = (function (_super) {
    __extends(Input, _super);
    function Input(element) {
        var _this = _super.call(this) || this;
        _this.onKeyUp = function (e) {
            if (e.which === 13 && _this.onEnter) {
                _this.onEnter(e);
            }
        };
        _this.element = element;
        _this.element.addEventListener('keyup', _this.onKeyUp);
        return _this;
    }
    Input.prototype.getElement = function () {
        return this.element;
    };
    Input.prototype.getValue = function () {
        return this.element.value;
    };
    Input.prototype.dispose = function () {
        this.element.removeEventListener('click', this.onKeyUp);
        this.element = null;
    };
    return Input;
}(Control));
/// <reference path="Control.ts" />
var Link = (function (_super) {
    __extends(Link, _super);
    function Link(element) {
        var _this = _super.call(this) || this;
        _this.clicked = function (e) {
            _this.onClick(e);
        };
        _this.element = element;
        _this.element.href = "javascript:;";
        _this.element.addEventListener('click', _this.clicked);
        return _this;
    }
    Link.prototype.getElement = function () {
        return this.element;
    };
    Link.prototype.dispose = function () {
        this.element.removeEventListener('click', this.clicked);
        this.element = null;
    };
    return Link;
}(Control));
/// <reference path="Controller.ts" />
/// <reference path="../Controls/Input.ts" />
/// <reference path="../Controls/Link.ts" />
/// <reference path="../RestClient.ts" />
/// <reference path="../SPA.ts" />
var LoginController = (function (_super) {
    __extends(LoginController, _super);
    function LoginController(container) {
        var _this = _super.call(this, container) || this;
        var submit = function (e) {
            RestClient.login({
                username: _this.email.getValue(),
                password: _this.password.getValue()
            }, function (res) {
                if (res.code === 200) {
                    Session.setToken(res.text);
                    SPA.popToHome();
                }
                else {
                    alert('Code: ' + res.code + '\n' + res.text);
                }
            });
        };
        _this.email = new Input(_this.getElementByControlId('email'));
        _this.password = new Input(_this.getElementByControlId('password'));
        _this.login = new Link(_this.getElementByControlId('login'));
        _this.email.onEnter = submit;
        _this.password.onEnter = submit;
        _this.login.onClick = submit;
        return _this;
    }
    LoginController.prototype.onActivated = function () {
        console.log('LoginController onActivated()');
    };
    LoginController.prototype.clean = function () {
        this.login.dispose();
        this.email.dispose();
        this.password.dispose();
    };
    return LoginController;
}(Controller));
/// <reference path="Controller.ts" />
/// <reference path="../Controls/Link.ts" />
/// <reference path="../Controls/Input.ts" />
/// <reference path="../RestClient.ts" />
var RegisterController = (function (_super) {
    __extends(RegisterController, _super);
    function RegisterController(container) {
        var _this = _super.call(this, container) || this;
        var submit = function (e) {
            RestClient.register({
                username: _this.email.getValue(),
                password: _this.password.getValue()
            }, function (res) {
                if (res.code === 200) {
                    alert('Successfully registered');
                    SPA.popToHome();
                }
                else {
                    alert('Code: ' + res.code + '\n' + res.text);
                }
            });
        };
        _this.email = new Input(_this.getElementByControlId('email'));
        _this.password = new Input(_this.getElementByControlId('password'));
        _this.registerLink = new Link(_this.getElementByControlId('register'));
        _this.registerLink.getElement().textContent = 'Register';
        _this.registerLink.onClick = submit;
        _this.email.onEnter = submit;
        _this.password.onEnter = submit;
        return _this;
    }
    RegisterController.prototype.onActivated = function () {
        console.log('RegisterController onActivated()');
    };
    RegisterController.prototype.clean = function () {
        this.registerLink.dispose();
        this.email.dispose();
        this.password.dispose();
    };
    return RegisterController;
}(Controller));
/// <reference path="Control.ts" />
var Table = (function (_super) {
    __extends(Table, _super);
    function Table(element) {
        var _this = _super.call(this) || this;
        _this.element = element;
        _this.addedRows = [];
        return _this;
    }
    Table.prototype.getElement = function () {
        return this.element;
    };
    Table.prototype.addRow = function (cols) {
        if (cols.length === 0) {
            return;
        }
        var tr = this.element.insertRow();
        for (var _i = 0, cols_1 = cols; _i < cols_1.length; _i++) {
            var col = cols_1[_i];
            var td = tr.insertCell();
            if (col instanceof HTMLElement) {
                td.appendChild(col);
            }
            else {
                td.textContent = col;
            }
        }
        this.addedRows.push(tr);
    };
    Table.prototype.getRowElement = function (index) {
        if (index >= this.addedRows.length || index < 0) {
            throw new Error('index out of bounds');
        }
        return this.addedRows[index];
    };
    Table.prototype.clearAddedRows = function () {
        while (this.addedRows.length > 0) {
            this.element.deleteRow(this.element.rows.length - 1);
            this.addedRows.pop();
        }
    };
    Table.prototype.dispose = function () {
        this.element = null;
    };
    return Table;
}(Control));
/// <reference path="Controller.ts" />
/// <reference path="../Controls/Table.ts" />
/// <reference path="../RestClient.ts" />
var UserInfoController = (function (_super) {
    __extends(UserInfoController, _super);
    function UserInfoController(container) {
        var _this = _super.call(this, container) || this;
        _this.userTable = new Table(_this.getElementByControlId('user-table'));
        return _this;
    }
    UserInfoController.prototype.onActivated = function () {
        var _this = this;
        console.log('UserInfoController onActivated');
        RestClient.getUser(Session.getToken(), function (res) {
            if (res.code === 200) {
                var data = JSON.parse(res.text);
                _this.userTable.addRow([data.wins, data.losses]);
            }
            else {
                alert('Code: ' + res.code + '\n' + res.text);
            }
        });
    };
    UserInfoController.prototype.clear = function () {
        this.userTable.dispose();
    };
    return UserInfoController;
}(Controller));
/// <reference path="Control.ts" />
/// <reference path="Link.ts" />
var Menu = (function (_super) {
    __extends(Menu, _super);
    function Menu(element) {
        var _this = _super.call(this) || this;
        _this.element = element;
        _this.menuItems = [];
        return _this;
    }
    Menu.prototype.getElement = function () {
        return this.element;
    };
    Menu.prototype.addMenuItem = function (item) {
        this.menuItems.push(item);
        var li = document.createElement('li');
        li.appendChild(item.getElement());
        this.element.appendChild(li);
    };
    Menu.prototype.removeMenuItem = function (item) {
        for (var i = 0; i < this.menuItems.length; i++) {
            if (this.menuItems[i] === item) {
                this.element.removeChild(item.getElement().parentElement);
                this.menuItems.splice(i, 1)[0].dispose();
                break;
            }
        }
    };
    Menu.prototype.dispose = function () {
        for (var _i = 0, _a = this.menuItems; _i < _a.length; _i++) {
            var item = _a[_i];
            item.dispose();
        }
        this.element = null;
    };
    return Menu;
}(Control));
/// <reference path="Controller.ts" />
/// <reference path="../Controls/Table.ts" />
/// <reference path="../Controls/Link.ts" />
/// <reference path="../Session.ts" />
/// <reference path="../RestClient.ts" />
var GameController = (function (_super) {
    __extends(GameController, _super);
    function GameController(container) {
        var _this = _super.call(this, container) || this;
        _this.submit = function (e) {
            if (_this.updating) {
                alert('Wait for opponent.');
                return;
            }
            if (_this.currentGuess.indexOf(-1) >= 0) {
                alert('Missing pins in guess.');
                return;
            }
            alert('Guess submitted.. Wait for opponent.');
            RestClient.submit(Session.getToken(), Session.getExtra('GameID'), _this.currentGuess, function (res) {
                if (res.code === 200) {
                    _this.update();
                }
                else {
                    alert('Waiting for opponent.');
                }
            });
        };
        _this.update = function () {
            _this.updating = true;
            RestClient.update(Session.getToken(), Session.getExtra('GameID'), function (res) {
                if (res.code === 200) {
                    var json = JSON.parse(res.text);
                    var colors = [];
                    console.log(json);
                    if (json.host.id === Session.getDecodedToken().uid) {
                        colors = json.host.turns[_this.currentTurn].correctGuess;
                    }
                    else {
                        colors = json.guest.turns[_this.currentTurn].correctGuess;
                    }
                    _this.setPins(_this.currentTurn, colors);
                    _this.boardTable.getRowElement(_this.currentTurn).classList.remove(GameController.CSS_CURRENT_TURN);
                    _this.currentTurn++;
                    _this.currentGuess = [-1, -1, -1, -1];
                    _this.updating = false;
                    _this.boardTable.getRowElement(_this.currentTurn).classList.add(GameController.CSS_CURRENT_TURN);
                }
                else if (res.code === 420) {
                    _this.update();
                }
                else {
                    _this.update();
                    //alert('Other player left game.');
                    //SPA.popToHome();
                }
            });
        };
        _this.paletteTable = new Table(_this.getElementByControlId('palette'));
        _this.paletteButtons = [];
        _this.currentColor = -1;
        _this.opponentPins = new Table(_this.getElementByControlId('opponent'));
        _this.boardTable = new Table(_this.getElementByControlId('board'));
        _this.boardButtons = [];
        _this.boardPins = [];
        _this.submitButton = new Link(_this.getElementByControlId('submit'));
        _this.submitButton.onClick = _this.submit;
        _this.currentTurn = 0;
        _this.currentGuess = [-1, -1, -1, -1];
        _this.updating = false;
        _this.createOpponentPins();
        _this.createPalette();
        _this.createBoard();
        return _this;
    }
    GameController.prototype.setPins = function (turn, colors) {
        for (var i = 0; i < colors.length; i++) {
            var pin = this.boardPins[turn][i];
            pin.classList.remove(GameController.CSS_UNSET_COLOR);
            var color = '';
            if (colors[i] === 2) {
                color = GameController.CSS_CORRECT_PLACE;
            }
            else if (colors[i] === 1) {
                color = GameController.CSS_CORRECT_COLOR;
            }
            pin.classList.add(color);
        }
    };
    GameController.prototype.createOpponentPins = function () {
        var json = Session.getExtra('GameInfo');
        var spans = [];
        var text = document.createElement('span');
        text.textContent = 'Opponent pins: ';
        spans.push(text);
        for (var _i = 0, _a = json.colorCodes; _i < _a.length; _i++) {
            var color = _a[_i];
            var span = document.createElement('span');
            span.classList.add('badge', GameController.CSS_COLORS[color]);
            span.appendChild(document.createElement('br'));
            spans.push(span);
        }
        this.opponentPins.addRow(spans);
    };
    GameController.prototype.createPalette = function () {
        var _this = this;
        var _loop_1 = function (i) {
            var cName = GameController.CSS_COLORS[i];
            var elem = document.createElement('a');
            elem.classList.add(cName, 'btn', 'btn-circle', 'btn-lg');
            var button = new Link(elem);
            button.onClick = function (e) {
                _this.currentColor = i;
            };
            this_1.paletteButtons.push(button);
            this_1.paletteTable.addRow([button.getElement()]);
        };
        var this_1 = this;
        for (var i = 0; i < GameController.CSS_COLORS.length; i++) {
            _loop_1(i);
        }
    };
    GameController.prototype.createBoard = function () {
        var _this = this;
        for (var i = 0; i < GameController.NUM_TURNS; i++) {
            var buttonRow = [];
            var contentRow = [];
            contentRow.push(i.toString());
            var _loop_2 = function (j) {
                var elem = document.createElement('a');
                elem.classList.add(GameController.CSS_UNSET_COLOR, 'btn', 'btn-circle', 'btn-lg');
                var button = new Link(elem);
                button.onClick = function (e) {
                    if (_this.currentColor === -1 ||
                        _this.currentTurn < 0 ||
                        _this.currentTurn > GameController.NUM_TURNS ||
                        _this.boardButtons[_this.currentTurn].indexOf(button) < 0) {
                        return;
                    }
                    var classList = button.getElement().classList;
                    classList.remove.apply(classList, GameController.CSS_COLORS);
                    classList.remove(GameController.CSS_UNSET_COLOR);
                    classList.add(GameController.CSS_COLORS[_this.currentColor]);
                    _this.currentGuess[j] = _this.currentColor;
                };
                buttonRow.push(button);
                contentRow.push(button.getElement());
            };
            for (var j = 0; j < 4; j++) {
                _loop_2(j);
            }
            contentRow.push(this.createPins());
            this.boardButtons.push(buttonRow);
            this.boardTable.addRow(contentRow);
        }
        this.boardTable.getRowElement(0).classList.add(GameController.CSS_CURRENT_TURN);
    };
    GameController.prototype.createPins = function () {
        var div = document.createElement('div');
        var row = [];
        for (var i = 0; i < 4; i++) {
            var span = document.createElement('span');
            span.classList.add('badge', GameController.CSS_UNSET_COLOR);
            span.appendChild(document.createElement('br'));
            div.appendChild(span);
            row.push(span);
        }
        this.boardPins.push(row);
        return div;
    };
    GameController.prototype.onActivated = function () {
        console.log('GameController onActivated()');
    };
    GameController.prototype.onDestroyed = function () {
    };
    GameController.prototype.clean = function () {
        this.paletteTable.dispose();
        this.opponentPins.dispose();
        this.boardTable.dispose();
        for (var _i = 0, _a = this.paletteButtons; _i < _a.length; _i++) {
            var button = _a[_i];
            button.dispose();
        }
        for (var _b = 0, _c = this.boardButtons; _b < _c.length; _b++) {
            var row = _c[_b];
            for (var _d = 0, row_1 = row; _d < row_1.length; _d++) {
                var button = row_1[_d];
                button.dispose();
            }
        }
        this.submitButton.dispose();
    };
    return GameController;
}(Controller));
GameController.CSS_COLORS = [
    'blue',
    'green',
    'lightblue',
    'yellow',
    'red',
    'purple',
    'grey',
    'black',
];
GameController.CSS_UNSET_COLOR = 'lightgrey';
GameController.CSS_CURRENT_TURN = 'current-turn';
GameController.CSS_CORRECT_COLOR = 'yellow';
GameController.CSS_CORRECT_PLACE = 'red';
GameController.NUM_TURNS = 10;
/// <reference path="Controller.ts" />
/// <reference path="GameController.ts" />
/// <reference path="../Controls/Table.ts" />
/// <reference path="../RestClient.ts" />
var LobbyController = (function (_super) {
    __extends(LobbyController, _super);
    function LobbyController(container) {
        var _this = _super.call(this, container) || this;
        _this.refreshPlayerlist = function () {
            _this.refreshTimeout = setTimeout(_this.refreshPlayerlist, 1000);
            RestClient.getHostedGame(Session.getToken(), Session.getExtra('GameID'), function (res) {
                if (res.code === 200) {
                    var json = JSON.parse(res.text);
                    if (!_this.prevJSON) {
                        _this.prevJSON = {
                            hostid: '',
                            guestid: ''
                        };
                    }
                    if (_this.prevJSON) {
                        if (json.hostid !== _this.prevJSON.hostid && json.guestid !== _this.prevJSON.guestid) {
                            if (json.hostid === Session.getDecodedToken().uid && json.guestid) {
                                _this.startGameLink.show();
                            }
                            else {
                                _this.startGameLink.hide();
                                if (json.guestid === Session.getDecodedToken().uid) {
                                    _this.wait();
                                }
                            }
                            var addRow = function (res2) {
                                if (res2.code === 200) {
                                    _this.playerTable.addRow([res2.text]);
                                }
                            };
                            _this.playerTable.clearAddedRows();
                            RestClient.getUserName(Session.getToken(), json.hostid, addRow);
                            if (json.guestid) {
                                RestClient.getUserName(Session.getToken(), json.guestid, addRow);
                            }
                        }
                    }
                }
                else {
                    console.log('1'); //alert('Lost connection to lobby.');
                    //SPA.popToHome();
                }
            });
        };
        _this.wait = function () {
            _this.clearTimeout();
            RestClient.wait(Session.getToken(), Session.getExtra('GameID'), function (res) {
                if (res.code === 200) {
                    Session.setExtra('GameInfo', JSON.parse(res.text));
                    SPA.popToHome();
                    SPA.loadTemplate('/templates/game.html', SPA.ViewType.Page, GameController);
                }
                else if (res.code === 404) {
                    console.log('2'); //alert('Lost connection to lobby.');
                    //SPA.popToHome();
                }
                else if (res.code === 420) {
                    _this.wait();
                }
            });
        };
        _this.startGameLink = new Link(_this.getElementByControlId('start'));
        _this.startGameLink.hide();
        _this.startGameLink.onClick = function (e) {
            RestClient.start(Session.getToken(), Session.getExtra('GameID'), function (res) {
                if (res.code == 200) {
                    Session.setExtra('GameInfo', JSON.parse(res.text));
                    SPA.popToHome();
                    SPA.loadTemplate('templates/game.html', SPA.ViewType.Page, GameController);
                }
                else {
                    alert('Cant start yet.');
                }
            });
        };
        _this.playerTable = new Table(_this.getElementByControlId('player-table'));
        _this.refreshTimeout = 0;
        return _this;
    }
    LobbyController.prototype.onActivated = function () {
        console.log('LobbyController onActivated');
        this.refreshPlayerlist();
    };
    LobbyController.prototype.clearTimeout = function () {
        if (this.refreshTimeout !== 0) {
            clearTimeout(this.refreshTimeout);
            this.refreshTimeout = 0;
        }
    };
    LobbyController.prototype.onDestroyed = function () {
        this.clearTimeout();
        RestClient.leave(Session.getToken(), Session.getExtra('GameID'), function (res) {
        });
    };
    LobbyController.prototype.clean = function () {
        this.startGameLink.dispose();
        this.playerTable.dispose();
    };
    return LobbyController;
}(Controller));
/// <reference path="Controller.ts" />
/// <reference path="../RestClient.ts" />
/// <reference path="../Controls/Table.ts" />
/// <reference path="../Controls/Link.ts" />
/// <reference path="LobbyController.ts" />
var HostListController = (function (_super) {
    __extends(HostListController, _super);
    function HostListController(container) {
        var _this = _super.call(this, container) || this;
        _this.hostGame = new Link(_this.getElementByControlId('host'));
        _this.hostGame.onClick = function (e) {
            RestClient.hostGame(Session.getToken(), function (res) {
                if (res.code === 200) {
                    var data = JSON.parse(res.text);
                    Session.setExtra('GameID', data.gameid);
                    SPA.loadTemplate('templates/lobby.html', SPA.ViewType.Page, LobbyController);
                }
            });
        };
        _this.refresh = new Link(_this.getElementByControlId('refresh'));
        _this.refresh.onClick = function (e) {
            _this.onActivated();
        };
        _this.hostedGames = new Table(_this.getElementByControlId('game-table'));
        _this.gameLinks = [];
        return _this;
    }
    HostListController.prototype.onActivated = function () {
        var _this = this;
        console.log('HostListController onActivated()');
        this.hostedGames.clearAddedRows();
        RestClient.getHostedGames(Session.getToken(), function (res) {
            if (res.code === 200) {
                var json = JSON.parse(res.text);
                var _loop_3 = function (key) {
                    var link = new Link(document.createElement('a'));
                    link.getElement().textContent = key;
                    link.onClick = function (e) {
                        RestClient.joinGame(Session.getToken(), key, function (joinRes) {
                            if (joinRes.code === 200) {
                                Session.setExtra('GameID', key);
                                SPA.loadTemplate('templates/lobby.html', SPA.ViewType.Page, LobbyController);
                            }
                            else {
                                alert('Could not join game. Code: ' + joinRes.code);
                            }
                        });
                    };
                    _this.gameLinks.push(link);
                    RestClient.getUserName(Session.getToken(), json[key].hostid, function (userRes) {
                        if (userRes.code === 200) {
                            _this.hostedGames.addRow([link.getElement(), userRes.text]);
                        }
                        else {
                            alert('Code: ' + userRes.code + '\n' + userRes.text);
                        }
                    });
                };
                for (var key in json) {
                    _loop_3(key);
                }
            }
            else {
                alert('Code: ' + res.code + '\n' + res.text);
            }
        });
    };
    HostListController.prototype.clean = function () {
        this.hostGame.dispose();
        this.refresh.dispose();
        this.hostedGames.dispose();
        for (var _i = 0, _a = this.gameLinks; _i < _a.length; _i++) {
            var link = _a[_i];
            link.dispose();
        }
    };
    return HostListController;
}(Controller));
/// <reference path="Controller.ts" />
/// <reference path="LoginController.ts" />
/// <reference path="RegisterController.ts" />
/// <reference path="UserInfoController.ts" />
/// <reference path="../Controls/Link.ts" />
/// <reference path="../Session.ts" />
/// <reference path="../Controls/Link.ts" />
/// <reference path="../Controls/Menu.ts" />
/// <reference path="HostListController.ts" />
var MainController = (function (_super) {
    __extends(MainController, _super);
    function MainController(container) {
        var _this = _super.call(this, container) || this;
        _this.keepAlive = function () {
            var timeout = setTimeout(_this.keepAlive, 10000);
            RestClient.ping(Session.getToken(), function (res) {
                if (res.code !== 200) {
                    clearTimeout(timeout);
                    alert('Connection to server lost/session expired');
                    Session.removeToken();
                    window.location.href = '/';
                }
            });
        };
        _this.homeLink = new Link(_this.getElementByControlId('home-link'));
        _this.homeLink.onClick = function (e) {
            SPA.popToHome();
        };
        _this.mainMenu = new Menu(_this.getElementByControlId('main-menu'));
        return _this;
    }
    MainController.prototype.onActivated = function () {
        var _this = this;
        console.log('MainController onActivated()');
        if (!Session.isAuthenticated()) {
            if (!this.loginLink) {
                this.loginLink = new Link(document.createElement('a'));
                this.loginLink.getElement().textContent = 'Login';
                this.loginLink.onClick = function (e) {
                    if (!(SPA.getTopController() instanceof LoginController)) {
                        SPA.loadTemplate('templates/login.html', SPA.ViewType.Page, LoginController);
                    }
                };
                this.mainMenu.addMenuItem(this.loginLink);
            }
            if (!this.registerLink) {
                this.registerLink = new Link(document.createElement('a'));
                this.registerLink.getElement().textContent = 'Register';
                this.registerLink.onClick = function (e) {
                    if (!(SPA.getTopController() instanceof RegisterController)) {
                        SPA.loadTemplate('templates/register.html', SPA.ViewType.Page, RegisterController);
                    }
                };
                this.mainMenu.addMenuItem(this.registerLink);
            }
        }
        else {
            this.keepAlive();
            this.mainMenu.removeMenuItem(this.loginLink);
            this.mainMenu.removeMenuItem(this.registerLink);
            if (!this.userLink) {
                this.userLink = new Link(document.createElement('a'));
                RestClient.getUser(Session.getToken(), function (res) {
                    if (res.code === 200) {
                        var json = JSON.parse(res.text);
                        _this.userLink.getElement().textContent = json.name;
                    }
                    else {
                        alert('Code: ' + res.code + '\n' + res.text);
                    }
                });
                this.userLink.onClick = function (e) {
                    if (!(SPA.getTopController() instanceof UserInfoController)) {
                        SPA.loadTemplate('templates/userinfo.html', SPA.ViewType.Page, UserInfoController);
                    }
                };
                this.mainMenu.addMenuItem(this.userLink);
            }
            if (!this.logoutLink) {
                this.logoutLink = new Link(document.createElement('a'));
                this.logoutLink.getElement().textContent = 'Logout';
                this.logoutLink.onClick = function (e) {
                    Session.removeToken();
                    window.location.href = '/';
                };
                this.mainMenu.addMenuItem(this.logoutLink);
            }
            SPA.loadTemplate('templates/hostlist.html', SPA.ViewType.Page, HostListController);
        }
    };
    MainController.prototype.clean = function () {
        this.homeLink.dispose();
        this.loginLink.dispose();
        this.registerLink.dispose();
        this.userLink.dispose();
        this.logoutLink.dispose();
        this.mainMenu.dispose();
    };
    return MainController;
}(Controller));
/// <reference path="SPA.ts" />
/// <reference path="Controllers/MainController.ts" />
document.addEventListener('DOMContentLoaded', function () {
    App.init();
});
var App;
(function (App) {
    function init() {
        SPA.init(document.getElementById('spa-container'), MainController);
    }
    App.init = init;
})(App || (App = {}));
