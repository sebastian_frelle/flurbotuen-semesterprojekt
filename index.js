"use strict";

var soap = require('strong-soap').soap;
var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var jwt = require('jsonwebtoken');
var config = require('./config');
const request = require('request')

var app = express();
var secureRouter = express.Router();
var openRouter = express.Router();

//app use
app.use(express.static('public'));
app.use(bodyParser.json());

// mongoose schemas
mongoose.connect(config.database);
mongoose.Promise = global.Promise;
var Schema = mongoose.Schema;
var userSchema = new Schema({
	name: { type: String, required: true },
	wins: { type: Number, default: 0 },
	losses: { type: Number, default: 0 }
});

var User = mongoose.model('User', userSchema);

// state variables
var activeGames = {}; // { 'gameid' : { 'host' : gameObject, 'guest' : gameObject } }
var hostedGames = {}; // {'gameid': id, 'hostid': id, 'guestid': id}
var colorCodes = {}; // { 'gameid': {'host' : [4], 'guest': [4]} }
var players = {};

// SOAP URL
var url = 'http://javabog.dk:9901/brugeradmin?wsdl';

secureRouter.use(function (req, res, next) {
	var token = req.headers['x-access-token'];

	if (token) {
		jwt.verify(token, config.secret, function (err, decoded) {
			if (err) {
				console.log(err);
				res.sendStatus(401);
			} else {
				req.decoded = decoded;
				next();
			}
		});
	} else {
		return res.sendStatus(401);
	}
});

app.use('/api', openRouter);
app.use('/api', secureRouter);

secureRouter.get('/getUser', function (req, res) {
	User.findOne({ _id: req.decoded.uid }, function (err, user) {
		if (err) {
			console.log(err);
			res.status(404).send('No user found');
			return;
		}

		res.status(200).json(user);
	});
});

secureRouter.get('/getUserName/:uid', function (req, res) {
	User.findOne({ _id: req.params.uid }, function (err, user) {
		if (err) {
			console.log(err);
			res.status(404).send('No user found');
			return;
		}

		res.status(200).send(user.name);
	});
});

openRouter.post('/login', function (req, res) {
	soap.createClient(url, function (err, client) {
		if (err) {
			console.log(err);
		}

		client.hentBruger({ arg0: req.body.username, arg1: req.body.password }, function (err, result) {
			if (err) {
				var options = {
					url: 'http://ubuntu4.javabog.dk:1440/login',
					method: 'POST',
					json: { username: req.body.username, password: req.body.password }
				}

				request(options, function (error, response, body) {
					if (error || body.status == 'fail') {
						console.log(error);
						res.status(404).send('Fejl ved login.');
						return;
					} else {
						login(req.body.username, res);
					}
				});

				return;
			}

			login(result.return.brugernavn, res);
			return;
		});
	});
});


function login(username, res) {
	var user = User({
		name: username
	});

	User.update(
		{ name: user.name },
		{ $setOnInsert: user },
		{ upsert: true },
		function (err, numAffected) {
			if (err) {
				console.log(err);
				return;
			}

			if (!numAffected.upsert) {
				User.findOne({ 'name': user.name }, function (err, foundUser) {
					if (err) {
						console.log(err);
						return;
					}

					var token = jwt.sign({ 'uid': foundUser._id }, config.secret, {
						expiresIn: 3600
					});

					players[foundUser._id] = { 'lastActivity': Date.now(), 'inGame': false }
					res.status(200).send(token);
				});
			} else {
				var token = jwt.sign({ 'uid': numAffected.upsert[0]._id }, config.secret, {
					expiresIn: 3600
				});

				players[numAffected.upsert[0]._id] = { 'lastActivity': Date.now(), 'inGame': false }
				res.status(200).send(token);
			}
		}
	);
}

// tells the server that the client is active
secureRouter.get('/ping', function (req, res) {
	if (!players[req.decoded.uid]) {
		res.sendStatus(404);
		return;
	}
	
	players[req.decoded.uid].lastActivity = Date.now();
	res.sendStatus(200);
});

// this call creates a new hosted game with a random game id
secureRouter.get('/host', function (req, res) {
	for (var property in hostedGames) {
		if (hostedGames[property].hostid == req.decoded.uid) {
			res.status(400).json({ response: 'already hosting a game' });
			return;
		}
	}

	while (true) {
		var randomId = Math.floor((Math.random() * 10000) + 1);

		if (!hostedGames[randomId] && !activeGames[randomId]) {
			hostedGames[randomId] = {
				hostid: req.decoded.uid,
				guestid: undefined
			};

			break;
		}
	}

	res.status(200).json({ gameid: randomId });
});

// returns a list of current hosted games
secureRouter.get('/hostlist', function (req, res) {
	res.status(200).json(hostedGames);
});

secureRouter.get('/getHostedGame/:gameid', function (req, res) {
	if (!hostedGames[req.params.gameid] ||
		(req.decoded.uid != hostedGames[req.params.gameid].guestid &&
		req.decoded.uid != hostedGames[req.params.gameid].hostid)) {
		res.sendStatus(401);
		return;
	}

	res.status(200).json(hostedGames[req.params.gameid]);
});

// the user joins a room with given game id
secureRouter.post('/join', function (req, res) {
	var gameid = req.body.gameid;

	if (!hostedGames[gameid]) {
		res.sendStatus(404);
		return;
	}

	if (hostedGames[gameid].guestid != null) {
		res.sendStatus(403);
		return;
	}

	var guest = req.decoded.uid;
	hostedGames[gameid].guestid = guest;

	res.sendStatus(200);
});

// called by the game host, starts a new active game and deletes the hosted one
secureRouter.post('/start', function (req, res) {
	var gameid = req.body.gameid;

	if (!hostedGames[gameid]) {
		res.sendStatus(404);
		return;
	}

	if (!hostedGames[gameid].guestid) {
		res.sendStatus(404);
		return;
	}

	var host = hostedGames[gameid].hostid;
	var guest = hostedGames[gameid].guestid;
	delete hostedGames[gameid];

	activeGames[gameid] = {
		host: {
			id: host,
			turns: []
		},
		guest: {
			id: guest,
			turns: []
		},
	};

	colorCodes[gameid] = {
		hostCodes: [],
		guestCodes: []
	}

	for (var i = 0; i < 4; i++) {
		colorCodes[gameid].hostCodes[i] = Math.floor(Math.random() * 8);
		colorCodes[gameid].guestCodes[i] = Math.floor(Math.random() * 8);
	}

	var data = {
		game: activeGames[gameid],
		colorCodes: colorCodes[gameid].hostCodes
	}

	res.status(200).json(data);
});

// called by the guest, asking the server if the game has started
secureRouter.post('/wait', function (req, res) {
	var gameid = req.body.gameid;
	
	var check = function (tries) {
		if (tries == 0) {
			res.sendStatus(420); //408 can't be used as this will cause browsers to keep resending the request.
			return;
		}

		if (activeGames[gameid]) {
			var data = {
				game: activeGames[gameid],
				colorCodes: colorCodes[gameid].guestCodes
			};

			res.status(200).json(data);
			return;
		} else if (!hostedGames[gameid]) {
			res.status(404).end();
			return;
		} else if ((Date.now() - players[hostedGames[gameid].hostid].lastActivity) > 15000) {
			res.sendStatus(404);
			return;
		} else if ((Date.now() - players[hostedGames[gameid].guestid].lastActivity) > 15000) {
			res.sendStatus(404);
			return;
		}

		if (tries > 0) {
			tries--;

			setTimeout(check, 1000, tries);
		}
	};

	check(10);
});

secureRouter.post('/submit', function (req, res) {
	var gameid = req.body.gameid;
	var guess = req.body.guess;
	var game, codes;

	if (activeGames[gameid].host.id == req.decoded.uid) {
		codes = colorCodes[gameid].guestCodes;
		game = activeGames[gameid].host;

		if (game.turns.length == 10 || game.turns.length > activeGames[gameid].guest.turns.length) {
			res.sendStatus(404);
			return;
		}
	} else if (activeGames[gameid].guest.id == req.decoded.uid) {
		codes = colorCodes[gameid].hostCodes;
		game = activeGames[gameid].guest;

		if (game.turns.length == 10 || game.turns.length > activeGames[gameid].host.turns.length) {
			res.sendStatus(404);
			return;
		}
	}

	var turn = { guess: guess, correctGuess: [] };
	var codeStatus = [0, 0, 0, 0];	// Already checked colors in code
	var guessStatus = [0, 0, 0, 0];	// Already checked colors in guess

	for (var i = 0; i < 4; i++) {
		var guessColor = guess[i];
		var index = codes.indexOf(guessColor);

		// Correct color
		if (index != -1) {

			// Correct placement
			if (i == index) {
				turn.correctGuess.push(2);
				codeStatus[index] = 1;
				guessStatus[i] = 1;
			}
		}
	}

	// If all colors aren't guessed
	if (turn.length != 4) {
		for (i = 0; i < 4; i++) {
			if (guessStatus[i] == 0) {
				guessColor = guess[i];
			} else {
				continue;
			}

			for (var j = 0; j < 4; j++) {
				if (codeStatus[j] == 0) {

					// Correct color but not placement
					if (guessColor == codes[j]) {
						turn.correctGuess.push(1);
						guessStatus[i] = 1;
						codeStatus[j] = 1;
					}
				}
			}
		}
	}

	game.turns.push(turn);
	res.sendStatus(200);
});

secureRouter.post('/update', function (req, res) {
	var gameid = req.body.gameid;

	var check = function (tries) {
		if (tries == 0) {
			res.sendStatus(420); //408 can't be used as this will cause browsers to keep resending the request.
			return;
		}

		if (activeGames[gameid].host.turns.length == activeGames[gameid].guest.turns.length) {
			res.status(200).json(activeGames[gameid]);
			return;
		}/* else if ((Date.now() - players[activeGames[gameid].hostid].lastActivity) > 15000) { //virker ikke
			res.sendStatus(404);
			return;
		} else if ((Date.now() - players[activeGames[gameid].guestid].lastActivity) > 15000) {
			res.sendStatus(404);
			return;
		}*/

		if (tries > 0) {
			tries--;

			setTimeout(check, 1000, tries);
		}
	};

	check(10);
});

// either removes the game or the guest depending on the caller
secureRouter.post('/leave', function (req, res) {
	var gameid = req.body.gameid;

	if (!hostedGames[gameid]) {
		res.sendStatus(404);
		return;
	}

	if (req.decoded.uid == hostedGames[gameid].hostid) {
		delete hostedGames[gameid];
	} else if (req.decoded.uid == hostedGames[gameid].guestid) {
		hostedGames[gameid].guestid = undefined;
	}

	res.sendStatus(200);
});

openRouter.post('/register', function (req, res) {
	var options = {
		url: 'http://ubuntu4.javabog.dk:1440/register',
		method: 'POST',
		json: { username: req.body.username, password: req.body.password }
	};

	request(options, function (error, response, body) {
		if (response.statusCode == 200) {
			res.sendStatus(200);
		} else {
			res.status(404);
		}
	});
});

app.listen(3000, function () {
	console.log('Server listening on port 3000');
});